# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

experimental-features-media-avif-description = Með þennan eiginleika virkan, styður { -brand-short-name } AV1 Image File (AVIF) sniðið. Þetta er kyrrmyndaskráasnið sem nýtir getu AV1 reiknirit myndskeiðsþjöppunar til að minnka myndstærð. Skoðaðu þessa <a data-l10n-name="bugzilla">villuskýrslu 1443863</a> til að sjá nánari upplýsingar.
# Do not translate 'SameSite', 'Lax' and 'None'.
experimental-features-cookie-samesite-lax-by-default2 =
    .label = Vefkökur: SameSite=Lax er sjálfgefið
# Fission is the name of the feature and should not be translated.
experimental-features-fission =
    .label = Fission (einangrun vefsvæðis)
experimental-features-fission-description = Fission (einangrun vefsvæðis) er tilraunaeiginleiki í { -brand-short-name } til að veita viðbótarlag af vörn gegn öryggisvillum. Með því að einangra hvet vefsvæði í sérstakt ferli, gerir Fission það erfiðara fyrir illgjörn vefsvæði að fá aðgang að upplýsingum af öðrum síðum sem þú heimsækir. Þetta er mikil breyting á uppbyggingu { -brand-short-name } og við myndum kunna að meta það að þú prófir og tilkynnir um vandamál sem þú gætir lent í. Fyrir frekari upplýsingar, skaltu skoða <a data-l10n-name="wiki">wiki</a>-síðurnar.
