# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

privatebrowsingpage-open-private-window-label = Opna huliðsglugga
    .accesskey = p
about-private-browsing-search-placeholder = Leita á vefnum
about-private-browsing-info-title = Þú ert í huliðsglugga
about-private-browsing-info-myths = Algengar mýtur um huliðsleit
about-private-browsing-search-btn =
    .title = Leita á vefnum
# Variables
#  $engine (String): the name of the user's default search engine
about-private-browsing-handoff =
    .title = Leitaðu með { $engine } eða settu inn vistfang
about-private-browsing-handoff-no-engine =
    .title = Leitaðu eða settu inn vistfang
# Variables
#  $engine (String): the name of the user's default search engine
about-private-browsing-handoff-text = Leitaðu með { $engine } eða settu inn vistfang
about-private-browsing-handoff-text-no-engine = Leitaðu eða settu inn vistfang
about-private-browsing-not-private = Þú ert ekki í huliðsglugga.
about-private-browsing-info-description = { -brand-short-name } hreinsar leitar- og vafraferilinn þinn þegar þú lokar forritinu eða lokar öllum huliðsflipum og gluggum. Þó að þetta sé ekki nafnlaust gagnvart vefsíðum eða þjónustuveitu þinni, gerir það auðveldara að halda því sem þú gerir á netinu hulið gagnvart einkaaðilum sem nota þessa tölvu.
about-private-browsing-need-more-privacy = Þarftu meira næði?
about-private-browsing-turn-on-vpn = Prófaðu { -mozilla-vpn-brand-name }
about-private-browsing-info-description-private-window = Huliðsgluggi: { -brand-short-name } hreinsar leitar- og vafraferilinn þinn þegar þú lokar öllum huliðsgluggum. Þetta gerir þig ekki nafnlausan á netinu.
about-private-browsing-info-description-simplified = { -brand-short-name } hreinsar leitar- og vafraferilinn þinn þegar þú lokar öllum huliðsgluggum. Þetta gerir þig ekki nafnlausan á netinu.
about-private-browsing-learn-more-link = Fræðast meira
about-private-browsing-hide-activity = Feldu virkni þína og staðsetningu, hvar sem þú vafrar
about-private-browsing-get-privacy = Fáðu persónuvernd hvar sem þú vafrar
about-private-browsing-hide-activity-1 = Feldu vafravirkni og staðsetningu með { -mozilla-vpn-brand-name }. Einn smellur útbýr örugga tengingu, jafnvel á þráðlausum Wi-Fi almenningsnetum.
about-private-browsing-prominent-cta = Haltu þig til hlés með { -mozilla-vpn-brand-name }
# This string is the title for the banner for search engine selection
# in a private window.
# Variables:
#   $engineName (String) - The engine name that will currently be used for the private window.
about-private-browsing-search-banner-title = { $engineName } er sjálfgefna leitarvélin þín í huliðsgluggum
about-private-browsing-search-banner-description =
    { PLATFORM() ->
        [windows] Til að velja aðra leitarvél skaltu fara í <a data-l10n-name="link-options">Valkostir</a>
       *[other] Til að velja aðra leitarvél skaltu fara í <a data-l10n-name="link-options">Kjörstillingar</a>
    }
about-private-browsing-search-banner-close-button =
    .aria-label = Loka
about-private-browsing-dismiss-button =
    .title = Hafna
