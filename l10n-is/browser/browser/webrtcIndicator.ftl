# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


# Note: This is currently placed under browser/base/content so that we can
# get the strings to appear without having our localization community need
# to go through and translate everything. Once these strings are ready for
# translation, we'll move it to the locales folder.

# This string is used so that the window has a title in tools that enumerate/look for window
# titles. It is not normally visible anywhere.
webrtc-indicator-title = { -brand-short-name } — Deilingarvísir
webrtc-sharing-window = Þú ert að deila öðrum forritsglugga.
webrtc-sharing-browser-window = Þú ert að deila { -brand-short-name }.
webrtc-sharing-screen = Þú ert að deila öllum skjánum þínum.
webrtc-stop-sharing-button = Hætta deilingu
webrtc-microphone-unmuted =
    .title = Slökkva á hljóðnema
webrtc-microphone-muted =
    .title = Kveikja á hljóðnema
webrtc-camera-unmuted =
    .title = Slökkva á myndavél
webrtc-camera-muted =
    .title = Kveikja á myndavél
webrtc-minimize =
    .title = Lágmarka vísi
# This string will display as a tooltip on supported systems where we show
# device sharing state in the OS notification area. We do not use these strings
# on macOS, as global menu bar items do not have native tooltips.
webrtc-camera-system-menu =
    .label = Þú ert að deila myndavélinni þinni. Smelltu til að stýra deilingu.
webrtc-microphone-system-menu =
    .label = Þú ert að deila hljóðnemanum þínum. Smelltu til að stýra deilingu.
webrtc-screen-system-menu =
    .label = Þú ert að deila glugga eða skjá. Smelltu til að stýra deilingu.
