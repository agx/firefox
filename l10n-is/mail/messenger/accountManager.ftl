# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

open-preferences-sidebar-button = Kjörstillingar { -brand-short-name }
open-preferences-sidebar-button2 = { -brand-short-name } stillingar
open-addons-sidebar-button = Viðbætur og þemu
account-action-add-newsgroup-account =
    .label = Bæta við reikningi fyrir fréttahóp…
    .accesskey = f
