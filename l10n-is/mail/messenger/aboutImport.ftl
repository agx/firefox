# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = Flytja inn

## Header

import-from-app = Flytja inn úr forriti
import-from-app-desc = Velja að flytja inn reikninga, nafnaskrár, dagatöl og önnur gögn frá:
import-address-book = Flytja inn nafnaskrá
import-calendar = Flytja inn dagatalsskrá

## Buttons

button-cancel = Hætta við
button-back = Til baka
button-continue = Halda áfram

## Import from app steps

# Variables:
#   $app (String) - The name of the app to import from
profiles-pane-title = Flytja inn úr { $app }
profiles-pane-desc = Veldu staðsetninguna sem á að flytja inn frá
profile-file-picker-dir = Veldu prófílmöppu
profile-file-picker-zip = Veldu zip-skrá (minni en 2GB)
items-pane-title = Veldu hvað á að flytja inn
items-pane-desc = Flytja inn úr
items-pane-checkbox-accounts = Reikningar og stillingar
items-pane-checkbox-address-books = Nafnaskrár
items-pane-checkbox-calendars = Dagatöl
items-pane-checkbox-mail-messages = Tölvupóstskilaboð

## Import dialog

progress-pane-title = Flyt inn
progress-pane-restart-desc = Endurræstu til að ljúka innflutningi.
error-pane-title = Villa
error-message-zip-file-too-big = Valin zip-skrá er stærri en 2GB. Afþjappaðu hana fyrst, síðan skaltu í staðinn flytja inn úr afþjöppuðu möppunni.
error-message-extract-zip-file-failed = Mistókst að afþjappa zip-skrána. Afþjappaðu hana handvirkt, síðan skaltu í staðinn flytja inn úr afþjöppuðu möppunni.
error-message-failed = Innflutningur mistókst óvænt, frekari upplýsingar gætu verið tiltækar í villuborðinu.
