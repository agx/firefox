<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY Contact.tab                     "Tengiliður">
<!ENTITY Contact.accesskey               "g">
<!ENTITY Name.box                        "Nafn">

<!-- LOCALIZATION NOTE:
 NameField1, NameField2, PhoneticField1, PhoneticField2
 those fields are either LN or FN depends on the target country.
 "FirstName" and "LastName" can be swapped for id to change the order
 but they should not be translated (same applied to phonetic id).
 Make sure the translation of label corresponds to the order of id.
-->

<!-- LOCALIZATION NOTE (NameField1.id) : DONT_TRANSLATE -->
<!ENTITY NameField1.id                  "FirstName">
<!-- LOCALIZATION NOTE (NameField2.id) : DONT_TRANSLATE -->
<!ENTITY NameField2.id                  "LastName">
<!-- LOCALIZATION NOTE (PhoneticField1.id) : DONT_TRANSLATE -->
<!ENTITY PhoneticField1.id              "PhoneticFirstName">
<!-- LOCALIZATION NOTE (PhoneticField2.id) : DONT_TRANSLATE -->
<!ENTITY PhoneticField2.id              "PhoneticLastName">

<!ENTITY NameField1.label               "Skírnarnafn:">
<!ENTITY NameField1.accesskey           "S">
<!ENTITY NameField2.label               "Föðurnafn:">
<!ENTITY NameField2.accesskey           "F">
<!ENTITY PhoneticField1.label           "Hljóðstafað nafn:">
<!ENTITY PhoneticField2.label           "Hljóðstafað nafn:">
<!ENTITY DisplayName.label              "Birting:">
<!ENTITY DisplayName.accesskey          "B">
<!ENTITY preferDisplayName.label        "Kjósið alltaf birtingarnafn fram yfir skilaboðahaus">
<!ENTITY preferDisplayName.accesskey    "j">
<!ENTITY NickName.label                 "Gælunafn:">
<!ENTITY NickName.accesskey             "N">

<!ENTITY PrimaryEmail.label             "Tölvupóstfang:">
<!ENTITY PrimaryEmail.accesskey         "p">
<!ENTITY SecondEmail.label              "Auka tölvupóstfang:">
<!ENTITY SecondEmail.accesskey          "a">
<!ENTITY PreferMailFormat.label         "Kýs að taka á móti skilaboðum sem eru sniðin sem:">
<!ENTITY PreferMailFormat.accesskey     "n">
<!ENTITY PlainText.label                "Ósniðinn texti">
<!ENTITY HTML.label                     "HTML">
<!ENTITY Unknown.label                  "Óþekkt">
<!ENTITY chatName.label                 "Nafn í spjalli:">

<!ENTITY WorkPhone.label                "Vinnusími:">
<!ENTITY WorkPhone.accesskey            "m">
<!ENTITY HomePhone.label                "Heima:">
<!ENTITY HomePhone.accesskey            "m">
<!ENTITY FaxNumber.label                "Fax:">
<!ENTITY FaxNumber.accesskey            "x">
<!ENTITY PagerNumber.label              "Símboði:">
<!ENTITY PagerNumber.accesskey          "b">
<!ENTITY CellularNumber.label           "Farsími:">
<!ENTITY CellularNumber.accesskey       "F">

<!ENTITY Home.tab                       "Einka">
<!ENTITY Home.accesskey                 "E">
<!ENTITY HomeAddress.label              "Heimilisfang:">
<!ENTITY HomeAddress.accesskey          "f">
<!ENTITY HomeAddress2.label             "">
<!ENTITY HomeAddress2.accesskey         "">
<!ENTITY HomeCity.label                 "Borg/Sveitarfélag:">
<!ENTITY HomeCity.accesskey             "B">
<!ENTITY HomeState.label                "Ríki/hérað:">
<!ENTITY HomeState.accesskey            "R">
<!ENTITY HomeZipCode.label              "Póstnúmer:">
<!ENTITY HomeZipCode.accesskey          "P">
<!ENTITY HomeCountry.label              "Land:">
<!ENTITY HomeCountry.accesskey          "n">
<!ENTITY HomeWebPage.label              "Vefsíða:">
<!ENTITY HomeWebPage.accesskey          "e">
<!ENTITY Birthday.label                 "Fæðingardagur:">
<!ENTITY Birthday.accesskey             "d">
<!ENTITY Month.placeholder              "Mánuður">
<!ENTITY Day.placeholder                "Dagur">
<!ENTITY Year.placeholder               "Ár">
<!ENTITY Age.label                      "Aldur:">
<!ENTITY Age.placeholder                "Aldur">

<!ENTITY Work.tab                       "Vinna">
<!ENTITY Work.accesskey                 "V">
<!ENTITY JobTitle.label                 "Titill:">
<!ENTITY JobTitle.accesskey             "i">
<!ENTITY Department.label               "Deild:">
<!ENTITY Department.accesskey           "D">
<!ENTITY Company.label                  "Fyrirtæki/Stofnun:">
<!ENTITY Company.accesskey              "n">
<!ENTITY WorkAddress.label              "Heimilisfang:">
<!ENTITY WorkAddress.accesskey          "H">
<!ENTITY WorkAddress2.label             "">
<!ENTITY WorkAddress2.accesskey         "">
<!ENTITY WorkCity.label                 "Borg/Sveitarfélag:">
<!ENTITY WorkCity.accesskey             "B">
<!ENTITY WorkState.label                "Ríki/Hérað:">
<!ENTITY WorkState.accesskey            "R">
<!ENTITY WorkZipCode.label              "Póstnúmer:">
<!ENTITY WorkZipCode.accesskey          "P">
<!ENTITY WorkCountry.label              "Land:">
<!ENTITY WorkCountry.accesskey          "L">
<!ENTITY WorkWebPage.label              "Vefsíða:">
<!ENTITY WorkWebPage.accesskey          "e">

<!ENTITY Other.tab                      "Annað">
<!ENTITY Other.accesskey                "A">
<!ENTITY Custom1.label                  "Sérsniðið 1:">
<!ENTITY Custom1.accesskey              "1">
<!ENTITY Custom2.label                  "Sérsniðið 2:">
<!ENTITY Custom2.accesskey              "2">
<!ENTITY Custom3.label                  "Sérsniðið 3:">
<!ENTITY Custom3.accesskey              "3">
<!ENTITY Custom4.label                  "Sérsniðið 4:">
<!ENTITY Custom4.accesskey              "4">
<!ENTITY Notes.label                    "Athugasemdir:">
<!ENTITY Notes.accesskey                "h">

<!ENTITY Chat.tab                       "Spjall">
<!ENTITY Chat.accesskey                 "a">
<!ENTITY Gtalk.label                    "Google Talk:">
<!ENTITY Gtalk.accesskey                "G">
<!ENTITY AIM.label                      "AIM:">
<!ENTITY AIM2.accesskey                 "M">
<!ENTITY Yahoo.label                    "Yahoo!:">
<!ENTITY Yahoo.accesskey                "Y">
<!ENTITY Skype.label                    "Skype:">
<!ENTITY Skype.accesskey                "S">
<!ENTITY QQ.label                       "QQ:">
<!ENTITY QQ.accesskey                   "Q">
<!ENTITY MSN.label                      "MSN:">
<!ENTITY MSN2.accesskey                 "N">
<!ENTITY ICQ.label                      "ICQ:">
<!ENTITY ICQ.accesskey                  "I">
<!ENTITY XMPP.label                     "Jabber auðkenni:">
<!ENTITY XMPP.accesskey                 "J">
<!ENTITY IRC.label                      "IRC gælunafn:">
<!ENTITY IRC.accesskey                  "R">

<!ENTITY Photo.tab                      "Mynd">
<!ENTITY Photo.accesskey                "y">
<!ENTITY GenericPhoto.label             "Almenn mynd">
<!ENTITY GenericPhoto.accesskey         "m">
<!ENTITY DefaultPhoto.label             "Sjálfgefið">
<!ENTITY PhotoFile.label                "Á þessari tölvu">
<!ENTITY PhotoFile.accesskey            "t">
<!ENTITY BrowsePhoto.label              "Velja">
<!ENTITY BrowsePhoto.accesskey          "l">
<!ENTITY PhotoURL.label                 "Á vefnum">
<!ENTITY PhotoURL.accesskey             "b">
<!ENTITY PhotoURL.placeholder           "Límdu eða settu inn veffang myndar">
<!ENTITY UpdatePhoto.label              "Uppfæra">
<!ENTITY UpdatePhoto.accesskey          "u">
<!ENTITY PhotoDropTarget.label          "Dragðu nýja mynd hingað">
