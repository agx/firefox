# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

upgrade-dialog-new-item-tabs-title = అధునాతన ట్యాబులు
upgrade-dialog-new-primary-default-button = { -brand-short-name }‌ను నా అప్రమేయ విహారిణిగా చేయి
upgrade-dialog-new-primary-theme-button = ఒక అలంకారాన్ని ఎంచుకోండి
upgrade-dialog-new-secondary-button = ఇప్పుడు కాదు
# This string is only shown on Windows 7, where we intentionally suppress the
# theme selection screen.
upgrade-dialog-new-primary-win7-button = సరే, అర్థమయ్యింది!

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).

upgrade-dialog-pin-secondary-button = ఇప్పుడు కాదు

## Default browser screen

upgrade-dialog-default-primary-button-2 = అప్రమేయ విహరిణిగా చేయి
upgrade-dialog-default-secondary-button = ఇప్పుడు కాదు

## Theme selection screen


## Start screen

# This title can be explicitly wrapped to control which words are on which line.
upgrade-dialog-start-title = జీవితం రంగులమయం
upgrade-dialog-start-secondary-button = ఇప్పుడు కాదు

## Colorway screen

upgrade-dialog-colorway-default-theme = అప్రమేయం
upgrade-dialog-theme-primary-button = అలంకారాన్ని భద్రపరుచు
upgrade-dialog-theme-secondary-button = ఇప్పుడు కాదు

## Thank you screen

upgrade-dialog-thankyou-primary-button = విహరించడం మొదలుపెట్టండి
