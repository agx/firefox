# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = Impordi

## Header

import-from-app = Rakendusest importimine
import-from-app-desc = Vali kontode, aadressiraamatute, kalendrite ja muude andmete importimine:
import-address-book = Aadressiraamatu faili importimine
import-calendar = Kalendri faili importimine

## Buttons

button-cancel = Loobu
button-back = Tagasi
button-continue = Jätka

## Import from app steps

# Variables:
#   $app (String) - The name of the app to import from
profiles-pane-title = Importimine rakendusest { $app }
profiles-pane-desc = Vali asukoht, kust importida
profile-file-picker-dir = Vali profiilikaust
profile-file-picker-zip = Vali ZIP-fail (väiksem kui 2GiB)
items-pane-title = Imporditava valimine
items-pane-desc = Imporditakse
items-pane-checkbox-accounts = kontod ja sätted
items-pane-checkbox-address-books = aadressiraamatud
items-pane-checkbox-calendars = kalendrid
items-pane-checkbox-mail-messages = e-post

## Import dialog

progress-pane-title = Importimine
progress-pane-restart-desc = Importimise lõpetamiseks taaskäivita.
error-pane-title = Viga
error-message-zip-file-too-big = Valitud ZIP-fail on suurem kui 2GiB. Palun paki see esmalt lahti ja impordi selle asemel lahtipakitud kaustast.
error-message-extract-zip-file-failed = ZIP-faili lahtipakkimine ebaõnnestus. Paki see käsitsi lahti ja impordi selle asemel lahtipakitud kaustast.
error-message-failed = Importimine ebaõnnestus ootamatult, rohkem infot võib olla saadaval veakonsoolis.
