# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

open-preferences-sidebar-button = { -brand-short-name }i sätted
open-preferences-sidebar-button2 = { -brand-short-name }i sätted
open-addons-sidebar-button = Lisad ja teemad
account-action-add-newsgroup-account =
    .label = Lisa uudistegruppide konto
    .accesskey = u
