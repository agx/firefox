# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = On aeg taaskäivitada brauser
restart-required-header = Vabandust. Meil on lihtsalt vaja tegevuse jätkamiseks üks väike asi ära teha.
restart-required-intro-brand = { -brand-short-name }i uuendati taustal. Uuenduse lõpuleviimiseks klõpsa Taaskäivita { -brand-short-name }.
restart-required-description = Kõik sinu avatud lehed, aknad ja kaardid taastatakse, et saaksid kiirelt tagasi oma tegevuste juurde.
restart-required-heading = { -brand-short-name }i kasutamise jätkamiseks taaskäivita
restart-required-intro = { -brand-short-name }i uuendamine algas taustal. Uuendamise lõpetamiseks peab taaskäivitama.
window-restoration-info = Aknad ja kaardid (välja arvatud privaatsed) taastatakse kiiresti.
restart-button-label = Taaskäivita { -brand-short-name }
