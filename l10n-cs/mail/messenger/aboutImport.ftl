# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = Import

## Header

import-from-app = Import z aplikace
import-from-app-desc = Zvolte, odkud chcete importovat účty, kontakty, kalendáře a další data:
import-address-book = Import souboru s kontakty
import-calendar = Import souboru s kalendářem

## Buttons

button-cancel = Zrušit
button-back = Zpět
button-continue = Pokračovat

## Import from app steps

# Variables:
#   $app (String) - The name of the app to import from
profiles-pane-title = Import z aplikace { $app }
profiles-pane-desc = Vyberte umístění, ze kterého chcete importovat
profile-file-picker-dir = Vyberte složku profilu
profile-file-picker-zip = Vyberte soubor ZIP (menší než 2 GB)
items-pane-title = Zvolte, co chcete importovat
items-pane-desc = Importovat z
items-pane-checkbox-accounts = Účty a nastavení
items-pane-checkbox-address-books = Kontakty
items-pane-checkbox-calendars = Kalendáře
items-pane-checkbox-mail-messages = E-mailové zprávy

## Import dialog

progress-pane-title = Probíhá import
progress-pane-restart-desc = Pro dokončení importu restartujte aplikaci.
error-pane-title = Chyba
error-message-zip-file-too-big = Vybraný soubor ZIP je větší než 2 GB. Nejprve ho prosím rozbalte na disk a poté importujte rozbalený adresář.
error-message-extract-zip-file-failed = Soubor ZIP se nepodařilo rozbalit. Rozbalte ho prosím ručně a naimportujte místo něj výslednou složku.
error-message-failed = Import se nepodařilo provést. Podrobnosti mohou být dostupné v chybové konzoli.
