# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = Імпорт

## Header

import-from-app = Імпорт із застосунку
import-from-app-desc = Виберіть імпортування облікових записів, адресних книг, календарів та інших даних із:
import-address-book = Імпорт файлу адресної книги
import-calendar = Імпорт файлу календаря

## Buttons

button-cancel = Скасувати
button-back = Назад
button-continue = Продовжити

## Import from app steps

# Variables:
#   $app (String) - The name of the app to import from
profiles-pane-title = Імпорт із { $app }
profiles-pane-desc = Виберіть звідки потрібно імпортувати
profile-file-picker-dir = Виберіть теку профілю
profile-file-picker-zip = Виберіть zip-файл (до 2 Гб)
items-pane-title = Виберіть, що імпортувати
items-pane-desc = Імпорт із
items-pane-checkbox-accounts = Облікові записи й налаштування
items-pane-checkbox-address-books = Адресні книги
items-pane-checkbox-calendars = Календарі
items-pane-checkbox-mail-messages = Повідомлення пошти

## Import dialog

progress-pane-title = Імпортування
progress-pane-restart-desc = Перезапустіть, щоб завершити імпортування.
error-pane-title = Помилка
error-message-zip-file-too-big = Вибраний zip-файл більший ніж 2 ГБ. Спочатку видобудьте його, а потім імпортуйте з видобутої теки.
error-message-extract-zip-file-failed = Не вдалося видобути zip-файл. Видобудьте його вручну, а потім імпортуйте з видобутої теки.
error-message-failed = Не вдалося імпортувати, можливо, більше даних буде доступно у Консолі помилок.
