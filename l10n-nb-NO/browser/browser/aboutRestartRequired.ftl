# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = Omstart kreves
restart-required-header = Beklager. Vi trenger bare å gjøre en liten ting for å fortsette.
restart-required-intro-brand = { -brand-short-name } har nettopp blitt oppdatert i bakgrunnen. Klikk på Start { -brand-short-name } på nytt for å fullføre oppdateringen.
restart-required-description = Vi vil gjenopprette alle sidene dine, vinduene og fanene etterpå, slik at du fort kan fortsette.
restart-required-heading = Start på nytt for å fortsette å bruke { -brand-short-name }
restart-required-intro = En oppdatering til { -brand-short-name } startet i bakgrunnen. Du må starte på nytt for å fullføre oppdateringen.
window-restoration-info = Vinduene og fanene dine vil raskt bli gjenopprettet, bortsett fra de private.
restart-button-label = Start { -brand-short-name } på nytt
