# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = Патрабуецца перазапуск
restart-required-header = Прабачце. Нам трэба зрабіць адну невялікую рэч, каб працягнуць працу.
restart-required-intro-brand = { -brand-short-name } толькі што абнавіўся ў фонавым рэжыме. Націсніце Перазапусціць { -brand-short-name } для завяршэння абнаўлення.
restart-required-description = Усе вашы старонкі, вокны і карткі будуць узноўлены, і вы зможаце хутка прадоўжыць свае справы.
restart-required-heading = Перазапусціць, каб працягваць карыстацца { -brand-short-name }
restart-required-intro = { -brand-short-name } абнаўляецца ў фонавым рэжыме. Каб завяршыць абнаўленне, вам трэба будзе перазапусціць браўзер.
window-restoration-info = Вашы вокны і карткі (акрамя прыватных) будуць хутка адноўлены.
restart-button-label = Перазапусціць { -brand-short-name }
