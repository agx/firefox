# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = Omstart påkravd
restart-required-header = Beklagar. Vi treng berre å gjere ein liten ting for å fortsetje.
restart-required-intro-brand = { -brand-short-name } har nettopp blitt oppdatert i bakgrunnen. Klikk på Start { -brand-short-name } på nytt for å fullføre oppdateringa.
restart-required-description = Vi vil gjenopprette alle sidene dine, vindauga og fanene etterpå, slik at du fort kan fortsetje.
restart-required-heading = Start på nytt for å halde fram med å bruke { -brand-short-name }
restart-required-intro = Ei oppdatering av { -brand-short-name } starta i bakgrunnen. Du må starte om for å fullføre uppdateringa.
window-restoration-info = Vindauga og fanene dine vil raskt bli gjenoppretta, bortsett frå dei private.
restart-button-label = Start om { -brand-short-name }
