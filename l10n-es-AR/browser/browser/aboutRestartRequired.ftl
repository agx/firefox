# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = Necesita reiniciar
restart-required-header = Disculpe. Sólo necesitamos hacer una pequeña cosa para continuar.
restart-required-intro-brand = { -brand-short-name } se acaba de actualizar en segundo plano. Haga clic en Reiniciar { -brand-short-name } para completar la actualización.
restart-required-description = Después restauraremos todas sus páginas, ventanas y pestañas para que pueda continuar rápidamente.
restart-required-heading = Reiniciar para continuar usando { -brand-short-name }
restart-required-intro = Una actualización a { -brand-short-name } comenzó en segundo plano. Tendrás que reiniciar para finalizar la actualización.
window-restoration-info = Tus ventanas y pestañas se restaurarán rápidamente, pero las privadas no.
restart-button-label = Reiniciar { -brand-short-name }
