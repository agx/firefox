# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = 再起動が必要です
restart-required-header = すみません。もう少しだけ操作が必要です。
restart-required-intro-brand = { -brand-short-name } はバックグラウンドで更新されました。{ -brand-short-name } の再起動をクリックすると更新が完了します。
restart-required-description = その後にページ、ウィンドウ、タブを復元しますので、すぐに作業に戻れます。

restart-required-heading = 引き続き使用するには { -brand-short-name } を再起動してください
restart-required-intro = バックグラウンドで { -brand-short-name } の更新を開始しました。更新を完了するには再起動が必要です。
window-restoration-info = ウィンドウとタブは速やかに復元されます。ただし、プライベートウィンドウは復元されません。

restart-button-label = { -brand-short-name } を再起動
