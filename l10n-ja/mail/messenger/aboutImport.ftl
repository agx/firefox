# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = 設定とデータのインポート

## Header

import-from-app = プログラムからインポート
import-from-app-desc = アカウント、アドレス帳、カレンダーおよび他のデータのインポート元を選んでください:
import-address-book = アドレス帳ファイルからインポート
import-calendar = カレンダーファイルからインポート

## Buttons

button-cancel = キャンセル
button-back = 戻る
button-continue = 次へ

## Import from app steps

# Variables:
#   $app (String) - The name of the app to import from
profiles-pane-title = { $app } からのインポート
profiles-pane-desc = インポート元の場所を選択してください
profile-file-picker-dir = プロファイルフォルダーを選択する
profile-file-picker-zip = zip ファイルを選択する (2GB サイズ制限)
items-pane-title = インポートする項目を選択してください
items-pane-desc = インポート元
items-pane-checkbox-accounts = アカウントと設定
items-pane-checkbox-address-books = アドレス帳
items-pane-checkbox-calendars = カレンダー
items-pane-checkbox-mail-messages = メールメッセージ

## Import dialog

progress-pane-title = インポートしています
progress-pane-restart-desc = 再起動してインポートを完了してください。
error-pane-title = エラー
error-message-zip-file-too-big = 選択された zip ファイルのサイズが 2GB を超えています。まず zip ファイルを展開し、その展開したフォルダーからデータをインポートしてください。
error-message-extract-zip-file-failed = zip ファイルの展開に失敗しました。手動で zip ファイルを展開し、その展開したフォルダーからデータをインポートしてください。
error-message-failed = 予期せずインポートに失敗しました。エラーの詳細はエラーコンソールに出力されています。
