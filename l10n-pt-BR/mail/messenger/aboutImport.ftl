# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = Importar

## Header

import-from-app = Importar do aplicativo
import-from-app-desc = Importar contas, catálogos de endereços, agendas e outros dados de:
import-address-book = Importar arquivo de catálogo de endereços
import-calendar = Importar arquivo de agenda

## Buttons

button-cancel = Cancelar
button-back = Voltar
button-continue = Continuar

## Import from app steps

# Variables:
#   $app (String) - The name of the app to import from
profiles-pane-title = Importar de { $app }
profiles-pane-desc = Escolha o local de onde importar
profile-file-picker-dir = Selecionar uma pasta de perfil
profile-file-picker-zip = Selecionar um arquivo zip (menor que 2GB)
items-pane-title = Selecione o que importar
items-pane-desc = Importar de
items-pane-checkbox-accounts = Contas e configurações
items-pane-checkbox-address-books = Catálogos de endereços
items-pane-checkbox-calendars = Agendas
items-pane-checkbox-mail-messages = Mensagens de email

## Import dialog

progress-pane-title = Importando
progress-pane-restart-desc = Reinicie para concluir a importação.
error-pane-title = Erro
error-message-zip-file-too-big = O arquivo zip selecionado tem mais de 2GB. Primeiro extraia o conteúdo, depois importe a partir da pasta onde foi extraído.
error-message-extract-zip-file-failed = Falha ao extrair o arquivo zip. Extraia manualmente, depois importe a partir da pasta onde foi extraído.
error-message-failed = A importação falhou inesperadamente; mais informações podem estar disponíveis no console de erros.
