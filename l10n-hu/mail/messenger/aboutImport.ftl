# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = Importálás

## Header

import-from-app = Importálás alkalmazásból
import-from-app-desc = Fiókok, címjegyzékek, naptárak és egyéb adatok importálása innen:
import-address-book = Címjegyzékfájl importálása
import-calendar = Naptárfájl importálása

## Buttons

button-cancel = Mégse
button-back = Vissza
button-continue = Folytatás

## Import from app steps

# Variables:
#   $app (String) - The name of the app to import from
profiles-pane-title = Importálás innen: { $app }
profiles-pane-desc = Válassza ki az importálás helyét
profile-file-picker-dir = Válasszon profilmappát
profile-file-picker-zip = Válasszon egy ZIP-fájlt (2 GB-nál kisebbet)
items-pane-title = Válasszon, hogy mit szeretne importálni
items-pane-desc = Importálás…
items-pane-checkbox-accounts = Fiókok és beállítások
items-pane-checkbox-address-books = Címjegyzékek
items-pane-checkbox-calendars = Naptárak
items-pane-checkbox-mail-messages = Levelek

## Import dialog

progress-pane-title = Importálás
progress-pane-restart-desc = Újraindítás az importálás befejezéséhez.
error-pane-title = Hiba
error-message-zip-file-too-big = A kiválasztott ZIP-fájl nagyobb, mint 2 GB. Először bontsa ki, majd importálja a kibontott mappából.
error-message-extract-zip-file-failed = A ZIP-fájl kibontása sikertelen. Bontsa ki kézzel, majd importálja a kibontott mappából.
error-message-failed = Az importálás váratlanul meghiúsult, további információ lehet elérhető a Hibakonzolban.
