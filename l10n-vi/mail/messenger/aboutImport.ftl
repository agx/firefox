# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = Nhập

## Header

import-from-app = Nhập từ ứng dụng
import-from-app-desc = Chọn để nhập tài khoản, sổ địa chỉ, lịch và các dữ liệu khác từ:
import-address-book = Nhập tập tin sổ địa chỉ
import-calendar = Nhập tập tin lịch

## Buttons

button-cancel = Hủy bỏ
button-back = Quay lại
button-continue = Tiếp tục

## Import from app steps

# Variables:
#   $app (String) - The name of the app to import from
profiles-pane-title = Nhập từ { $app }
profiles-pane-desc = Chọn vị trí để nhập
profile-file-picker-dir = Chọn một thư mục hồ sơ
profile-file-picker-zip = Chọn một tập tin zip (nhỏ hơn 2GB)
items-pane-title = Chọn những gì để nhập
items-pane-desc = Nhập từ
items-pane-checkbox-accounts = Tài khoản và cài đặt
items-pane-checkbox-address-books = Sổ địa chỉ
items-pane-checkbox-calendars = Lịch
items-pane-checkbox-mail-messages = Thư

## Import dialog

progress-pane-title = Đang nhập
progress-pane-restart-desc = Khởi động lại để hoàn tất quá trình nhập.
error-pane-title = Lỗi
error-message-zip-file-too-big = Tập tin zip đã chọn lớn hơn 2GB. Vui lòng giải nén nó trước, sau đó nhập từ thư mục đã giải nén.
error-message-extract-zip-file-failed = Không giải nén được tập tin zip. Vui lòng giải nén nó theo cách thủ công, sau đó nhập từ thư mục đã giải nén để thay thế.
error-message-failed = Nhập không thành công đột ngột, có thể xem thêm thông tin trong bảng điều khiển.
