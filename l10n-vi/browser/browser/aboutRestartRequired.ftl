# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = Yêu cầu khởi động lại
restart-required-header = Xin lỗi. Chúng tôi chỉ cần làm một việc nho nhỏ để tiếp tục.
restart-required-intro-brand = { -brand-short-name } vừa được cập nhật trong nền. Nhấp vào Khởi động lại { -brand-short-name } để hoàn thành cập nhật.
restart-required-description = Sau đó, chúng tôi sẽ khôi phục tất cả các trang, cửa sổ và thẻ của bạn để bạn có thể quay lại công việc của mình một cách nhanh chóng.
restart-required-heading = Khởi động lại để tiếp tục sử dụng { -brand-short-name }
restart-required-intro = Một bản cập nhật { -brand-short-name } đã chạy trong nền. Bạn cần phải khởi động lại để hoàn tất quá trình cập nhật.
window-restoration-info = Các cửa sổ và thẻ của bạn sẽ nhanh chóng được khôi phục, nhưng sẽ không với những cửa sổ riêng tư.
restart-button-label = Khởi động lại { -brand-short-name }
