# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = Tuo

## Header

import-from-app = Tuo sovelluksesta
import-address-book = Tuo osoitekirjatiedosto
import-calendar = Tuo kalenteritiedosto

## Buttons

button-cancel = Peruuta
button-back = Edellinen
button-continue = Jatka

## Import from app steps

# Variables:
#   $app (String) - The name of the app to import from
profiles-pane-title = Tuo sovelluksesta { $app }
profiles-pane-desc = Valitse sijainti, josta tuodaan
profile-file-picker-dir = Valitse profiilikansio
profile-file-picker-zip = Valitse zip-tiedosto (pienempi kuin 2 Gt)
items-pane-title = Valitse tuotavat asiat
items-pane-checkbox-accounts = Tilit ja asetukset
items-pane-checkbox-address-books = Osoitekirjat
items-pane-checkbox-calendars = Kalenterit
items-pane-checkbox-mail-messages = Sähköpostiviestit

## Import dialog

error-pane-title = Virhe
