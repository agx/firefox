# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## These strings are used in the about:preferences moreFromMozilla page

more-from-moz-title = Lisää { -vendor-short-name }lta
more-from-moz-category =
    .tooltiptext = Lisää { -vendor-short-name }lta
more-from-moz-firefox-mobile-description = Mobiiliselain, joka asettaa yksityisyytesi etusijalle.
more-from-moz-qr-code-box-firefox-mobile-title = Lataa mobiililaitteellasi. Suuntaa kamerasi QR-koodiin. Kun linkki tulee näkyviin, napauta sitä.
more-from-moz-qr-code-box-firefox-mobile-button = Lähetä sen sijaan sähköposti puhelimeesi
more-from-moz-button-mozilla-vpn =
    .label = Hanki VPN
more-from-moz-qr-code-firefox-mobile-img =
    .alt = QR-koodi { -brand-product-name }in mobiiliversion lataamiseksi
more-from-moz-button-mozilla-vpn-2 = Hanki VPN
more-from-moz-learn-more-link = Lue lisää
