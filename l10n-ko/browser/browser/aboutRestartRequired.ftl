# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = 다시 시작 필요
restart-required-header = 죄송합니다. 계속하기 위해서 처리할 일이 있습니다.
restart-required-intro-brand = { -brand-short-name }가 백그라운드에서 업데이트되었습니다. 업데이트를 완료하려면 { -brand-short-name } 다시 시작을 누르세요.
restart-required-description = 하던 일을 빨리 계속할 수 있게 하기 위해 모든 페이지와 창, 탭을 복원할 것입니다.
restart-required-heading = { -brand-short-name }를 계속 사용하려면 다시 시작
restart-required-intro = { -brand-short-name } 업데이트가 백그라운드에서 시작되었습니다. 업데이트를 완료하려면 다시 시작해야 합니다.
window-restoration-info = 일반 창과 탭은 즉시 복원되지만, 사생활 보호 창과 탭은 복원되지 않습니다.
restart-button-label = { -brand-short-name } 다시 시작
