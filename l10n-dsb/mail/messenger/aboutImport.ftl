# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = Importěrowaś

## Header

import-from-app = Z nałoženja importěrowaś
import-from-app-desc = Wubjeŕśo, wótkulž maju se konta, adresniki, kalendarje a druge daty importěrowaś:
import-address-book = Adresnikowu dataju importěrowaś
import-calendar = Dataju kalendarja importěrowaś

## Buttons

button-cancel = Pśetergnuś
button-back = Slědk
button-continue = Dalej

## Import from app steps

# Variables:
#   $app (String) - The name of the app to import from
profiles-pane-title = Z { $app } importěrowaś
profiles-pane-desc = Wubjeŕśo městno, wótkulž ma se importěrowaś
profile-file-picker-dir = Wubjeŕśo profilowy zarědnik
profile-file-picker-zip = Wubjeŕśo zip-dataju (mjeńšu ako 2 GB)
items-pane-title = Wubjeŕśo, což ma se importěrowaś
items-pane-desc = Importěrowaś z
items-pane-checkbox-accounts = Konta a nastajenja
items-pane-checkbox-address-books = Adresniki
items-pane-checkbox-calendars = Kalendarje
items-pane-checkbox-mail-messages = Mejlki

## Import dialog

progress-pane-title = Importěrowanje
progress-pane-restart-desc = Startujśo znowego, aby importěrowanje dokóńcył.
error-pane-title = Zmólka
error-message-zip-file-too-big = Wubrana zip-dataja jo wětša ako 2 GB. Pšosym rozpakujśo ju nejpjerwjej, a importěrujśo wopśimjeśe z rozpakowanego zarědnika město togo.
error-message-extract-zip-file-failed = Zip-dataja njedajo se rozpakowaś. Pšosym rozpakujśo ju manuelnje a importěrujśo ju pón město togo z ekstrahěrowanego zarědnika.
error-message-failed = Importěrowanje njejo se njenaźejucy raźiło, dalšne informacije namakajośo snaź w zmólkowej konsoli.
