# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = Importuj

## Header

import-from-app = Importuj z aplikacji
import-from-app-desc = Wybierz import kont, książek adresowych, kalendarzy i innych danych z:
import-address-book = Importuj plik książki adresowej
import-calendar = Importuj plik kalendarza

## Buttons

button-cancel = Anuluj
button-back = Wstecz
button-continue = Kontynuuj

## Import from app steps

# Variables:
#   $app (String) - The name of the app to import from
profiles-pane-title = Importuj z aplikacji { $app }
profiles-pane-desc = Wybierz położenie, z którego importować
profile-file-picker-dir = Wybierz folder profilu
profile-file-picker-zip = Wybierz plik ZIP (mniejszy niż 2 GB)
items-pane-title = Wybierz, co importować
items-pane-desc = Importuj z
items-pane-checkbox-accounts = Konta i ustawienia
items-pane-checkbox-address-books = Książki adresowe
items-pane-checkbox-calendars = Kalendarze
items-pane-checkbox-mail-messages = Wiadomości pocztowe

## Import dialog

progress-pane-title = Importowanie
progress-pane-restart-desc = Uruchom ponownie, aby dokończyć importowanie.
error-pane-title = Błąd
error-message-zip-file-too-big = Wybrany plik ZIP jest większy niż 2 GB. Najpierw go rozpakuj, a następnie zaimportuj z rozpakowanego folderu.
error-message-extract-zip-file-failed = Rozpakowanie pliku ZIP się nie powiodło. Rozpakuj go ręcznie, a następnie zaimportuj z rozpakowanego folderu.
error-message-failed = Import nieoczekiwanie się nie powiódł. Więcej informacji może być dostępnych w konsoli błędów.
