# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = Necessari da reaviar
restart-required-header = Perstgisa. Nus stuain be far ina piculezza avant che cuntinuar.
restart-required-intro-brand = { -brand-short-name } è gist vegnì actualisà davos las culissas. Cliccar sin «Reaviar { -brand-short-name }» per cumplettar l'actualisaziun.
restart-required-description = Tut tias paginas, fanestras e tes tabs vegnan restaurads suenter avair reavià per che ti possias cuntinuar senza perder temp.
restart-required-heading = Reaviar per cuntinuar cun { -brand-short-name }
restart-required-intro = Ina actualisaziun da { -brand-short-name } ha cumenzà davos las culissas. Ti vegns a stuair reaviar per terminar l'actualisaziun.
window-restoration-info = Tias fanestras e tes tabs vegnan subit restaurads, cun excepziun da quels anonims.
restart-button-label = Reaviar { -brand-short-name }
