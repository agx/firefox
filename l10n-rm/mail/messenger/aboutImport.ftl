# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = Importar

## Header

import-from-app = Importar dad ina applicaziun
import-from-app-desc = Tscherna dad importar contos, cudeschets d'adressas, chalenders ed autras datas da:
import-address-book = Importar ina datoteca cun in cudeschet d'adressas
import-calendar = Importar ina datoteca da chalender

## Buttons

button-cancel = Interrumper
button-back = Enavos
button-continue = Cuntinuar

## Import from app steps

# Variables:
#   $app (String) - The name of the app to import from
profiles-pane-title = Importar da { $app }
profiles-pane-desc = Tscherna la posiziun d'origin per l'import
profile-file-picker-dir = Tscherna in ordinatur da profil
profile-file-picker-zip = Tscherna ina datoteca zip (che na surpassa betg 2GB)
items-pane-title = Tscherna tge importar
items-pane-desc = Importar da
items-pane-checkbox-accounts = Contos e parameters
items-pane-checkbox-address-books = Cudeschets d'adressas
items-pane-checkbox-calendars = Chalenders
items-pane-checkbox-mail-messages = Messadis dad e-mail

## Import dialog

progress-pane-title = Importar
progress-pane-restart-desc = Reaviar per finir l'import.
error-pane-title = Errur
error-message-zip-file-too-big = La datoteca zip tschernida surpassa 2GB. L'extira l'emprim ed importescha lura ord l'ordinatur extratg.
error-message-extract-zip-file-failed = I n'è betg reussì dad extrair la datoteca zip. L'extira per plaschair manualmain ed importescha lura ord l'ordinatur extratg.
error-message-failed = L'import n'è betg reussì nunspetgadamain. Ulteriuras infurmaziuns stattan eventualmain a disposiziun en la consola d'errurs.
