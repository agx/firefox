# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = Importazione

## Header

import-from-app = Importa da applicazione
import-from-app-desc = Scegli di importare account, rubriche, calendari e altri dati da:
import-address-book = Importa file della rubrica
import-calendar = Importa file del calendario

## Buttons

button-cancel = Annulla
button-back = Indietro
button-continue = Continua

## Import from app steps

# Variables:
#   $app (String) - The name of the app to import from
profiles-pane-title = Importazione da { $app }
profiles-pane-desc = Scegli la posizione da cui importare
profile-file-picker-dir = Seleziona una cartella del profilo
profile-file-picker-zip = Seleziona un file zip (dimensione inferiore a 2 GB)
items-pane-title = Seleziona che cosa importare
items-pane-desc = Importa da
items-pane-checkbox-accounts = Account e impostazioni
items-pane-checkbox-address-books = Rubriche
items-pane-checkbox-calendars = Calendari
items-pane-checkbox-mail-messages = Messaggi di posta

## Import dialog

progress-pane-title = Importazione
progress-pane-restart-desc = Riavvia per completare l’importazione.
error-pane-title = Errore
error-message-zip-file-too-big = Il file ZIP selezionato è più grande di 2 GB. Estrarre i file e importarli dalla cartella estratta.
error-message-extract-zip-file-failed = Estrazione del file ZIP non riuscita. Estrarre i file manualmente, quindi importarli dalla cartella estratta.
error-message-failed = Importazione non riuscita per un errore inatteso; potrebbero essere disponibili ulteriori informazioni nella console degli errori.
