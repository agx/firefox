# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = 匯入

## Header

import-from-app = 從應用程式匯入
import-from-app-desc = 選擇從下列位置匯入帳號、通訊錄、行事曆與其他資料:
import-address-book = 匯入通訊錄檔案
import-calendar = 匯入行事曆檔案

## Buttons

button-cancel = 取消
button-back = 返回
button-continue = 繼續

## Import from app steps

# Variables:
#   $app (String) - The name of the app to import from
profiles-pane-title = 從 { $app } 匯入
profiles-pane-desc = 請選擇要從哪裡匯入資料
profile-file-picker-dir = 選擇設定檔資料夾
profile-file-picker-zip = 選擇 ZIP 檔（小於 2GB）
items-pane-title = 選擇要匯入的項目
items-pane-desc = 從下列位置匯入
items-pane-checkbox-accounts = 帳號與設定
items-pane-checkbox-address-books = 通訊錄
items-pane-checkbox-calendars = 行事曆
items-pane-checkbox-mail-messages = 郵件訊息

## Import dialog

progress-pane-title = 匯入中
progress-pane-restart-desc = 請重新啟動以完成匯入。
error-pane-title = 錯誤
error-message-zip-file-too-big = 選擇的 ZIP 檔案大小超過 2GB。請先解壓縮，然後改匯入解壓縮的資料匣。
error-message-extract-zip-file-failed = ZIP 檔解壓縮失敗，請手動解壓縮，並重新匯入解開的資料夾。
error-message-failed = 發生未預期的匯入失敗，錯誤主控台中可能有更多資訊。
