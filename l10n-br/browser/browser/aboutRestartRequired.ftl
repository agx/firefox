# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = Adloc’hañ dleet
restart-required-header = Digarezit. Ret eo deomp ober un draig ouzhpenn evit kenderc’hel.
restart-required-intro-brand = Hizivaet eo bet { -brand-short-name } en drekleur. Klikit war Adloc’hañ { -brand-short-name } evit echuiñ an hizivadenn.
restart-required-description = Assavet e vo ho holl bajennoù, prenestroù hag ivinelloù da c’houde, evit ma c’hallfec’h kenderc’hel buan.
restart-required-heading = Adloc'hit evit kenderc'hel da arverañ { -brand-short-name }
restart-required-intro = Kroget eo un hizivadenn eus { -brand-short-name } en drekleur. Ret e vo deoc'h adloc'hañ evit echuiñ an hizivadenn.
window-restoration-info = Assavet e vo ho prenestroù hag hoc'h ivinelloù, met ket ar re brevez.
restart-button-label = Adloc’hañ { -brand-short-name }
