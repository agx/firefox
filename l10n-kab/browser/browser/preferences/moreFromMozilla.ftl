# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## These strings are used in the about:preferences moreFromMozilla page

more-from-moz-button-mozilla-vpn =
    .label = Awi VPN
more-from-moz-button-mozilla-vpn-2 = Awi VPN
more-from-moz-learn-more-link = Issin ugar
