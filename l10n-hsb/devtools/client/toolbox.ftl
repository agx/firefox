# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### These messages are used in the DevTools toolbox.


## These labels are shown in the "..." menu in the toolbox, and represent different
## commands such as the docking of DevTools, toggling features, and viewing some
## external links. Some of the commands have the keyboard shortcut shown next to
## the label.

toolbox-meatball-menu-dock-bottom-label = Deleka přidokować
toolbox-meatball-menu-dock-left-label = Nalěwo přidokować
toolbox-meatball-menu-dock-right-label = Naprawo přidokować
toolbox-meatball-menu-dock-separate-window-label = Wosebite wokno
toolbox-meatball-menu-splitconsole-label = Rozdźělenu konsolu pokazać
toolbox-meatball-menu-hideconsole-label = Rozdźělenu konsolu schować
toolbox-meatball-menu-settings-label = Nastajenja
toolbox-meatball-menu-documentation-label = Dokumentacija…
toolbox-meatball-menu-community-label = Zhromadźenstwo…
# This menu item is only available in the browser toolbox. It forces the popups/panels
# to stay visible on blur, which is primarily useful for addon developers and Firefox
# contributors.
toolbox-meatball-menu-noautohide-label = Wuskakowace wokna awtomatisce njeschować
toolbox-meatball-menu-pseudo-locale-accented = Lokale  „z diakritiskim znamjenjom“ zmóžnić
toolbox-meatball-menu-pseudo-locale-bidi = Lokale „bidi“ zmóžnić

##

