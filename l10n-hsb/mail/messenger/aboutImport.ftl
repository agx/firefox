# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = Importować

## Header

import-from-app = Z nałoženja importować
import-from-app-desc = Wubjerće, zwotkelž maja so konta, adresniki, protyki a druhe daty importować:
import-address-book = Adresnikowu dataju importować
import-calendar = Dataju protyki importować

## Buttons

button-cancel = Přetorhnyć
button-back = Wróćo
button-continue = Dale

## Import from app steps

# Variables:
#   $app (String) - The name of the app to import from
profiles-pane-title = Z { $app } importować
profiles-pane-desc = Wubjerće městno, zwotkelž ma so importować
profile-file-picker-dir = Wubjerće profilowy rjadowak
profile-file-picker-zip = Wubjerće zip-dataju (mjeńšu hač 2 GB)
items-pane-title = Wubjerće, štož ma so importować
items-pane-desc = Importować z
items-pane-checkbox-accounts = Konta a nastajenja
items-pane-checkbox-address-books = Adresniki
items-pane-checkbox-calendars = Protyki
items-pane-checkbox-mail-messages = Mejlki

## Import dialog

progress-pane-title = Importowanje
progress-pane-restart-desc = Startujće znowa, zo byšće importowanje dokónčił.
error-pane-title = Zmylk
error-message-zip-file-too-big = Wubrana zip-dataja je wjetša hač 2 GB. Prošu rozpakujće ju najprjedy, a importujće wobsah z rozpakowaneho rjadowaka město toho.
error-message-extract-zip-file-failed = Zip-dataja njeda so rozbalić. Prošu wupakujće ju manuelnje a importujće ju potom město toho z ekstrahowaneho rjadowaka.
error-message-failed = Importowanje je so njenadźicy nimokuliło, dalše informacije namakaće snano w zmylkowej konsoli.
