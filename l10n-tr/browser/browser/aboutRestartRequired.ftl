# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = Yeniden başlatma gerekiyor
restart-required-header = Kusura bakmayın, devam etmeden önce küçük bir işimiz daha var.
restart-required-intro-brand = { -brand-short-name } arka planda güncellendi. Güncellemeyi tamamlamak için “{ -brand-short-name } tarayıcısını yeniden başlat”a tıklayın.
restart-required-description = Güncellemeden sonra hemen işinize dönebilmeniz için tüm sayfa, pencere ve sekmelerinizi geri yükleyeceğiz.
restart-required-heading = { -brand-short-name } tarayıcısını kullanmaya devam etmek için yeniden başlatın
restart-required-intro = Arka planda bir { -brand-short-name } güncellemesi başlatıldı. Güncellemeyi bitirmek için tarayıcıyı yeniden başlatmanız gerekiyor.
window-restoration-info = Pencereleriniz ve sekmeleriniz geri yüklenecek ama gizli pencereleriniz geri yüklenmeyecek.
restart-button-label = { -brand-short-name } tarayıcısını yeniden başlat
