# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = İçe aktar

## Header

import-from-app = Uygulamadan içe aktar
import-address-book = Adres defteri dosyasını içe aktar
import-calendar = Takvim dosyasını içe aktar

## Buttons

button-cancel = Vazgeç
button-back = Geri
button-continue = Devam et

## Import from app steps

# Variables:
#   $app (String) - The name of the app to import from
profiles-pane-title = { $app } uygulamasından içe aktar
profiles-pane-desc = İçe aktarılacak konumu seçin
profile-file-picker-dir = Bir profil klasörü seçin
profile-file-picker-zip = Bir zip dosyası seçin (2 GB'den küçük)
items-pane-title = Nelerin içe aktarılacağını seçin
items-pane-checkbox-accounts = Hesaplar ve ayarlar
items-pane-checkbox-address-books = Adres defterleri
items-pane-checkbox-calendars = Takvimler
items-pane-checkbox-mail-messages = Posta iletileri

## Import dialog

progress-pane-title = İçe aktarılıyor
progress-pane-restart-desc = İçe aktarmayı bitirmek için yeniden başlatın.
error-pane-title = Hata
error-message-extract-zip-file-failed = Zip dosyası çıkarılamadı. Lütfen elle çıkardıktan sonra klasörden içe aktarın.
error-message-failed = İçe aktarma beklenmedik bir şekilde başarısız oldu. Hata Konsolu'nda daha fazla bilgi mevcut olabilir.
