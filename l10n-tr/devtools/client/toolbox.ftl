# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### These messages are used in the DevTools toolbox.


## These labels are shown in the "..." menu in the toolbox, and represent different
## commands such as the docking of DevTools, toggling features, and viewing some
## external links. Some of the commands have the keyboard shortcut shown next to
## the label.

toolbox-meatball-menu-dock-bottom-label = Alt kenara sabitle
toolbox-meatball-menu-dock-left-label = Sola sabitle
toolbox-meatball-menu-dock-right-label = Sağa sabitle
toolbox-meatball-menu-dock-separate-window-label = Ayrı pencere
toolbox-meatball-menu-splitconsole-label = Ayrık konsolu göster
toolbox-meatball-menu-hideconsole-label = Ayrık konsolu gizle
toolbox-meatball-menu-settings-label = Ayarlar
toolbox-meatball-menu-documentation-label = Belgelendirme…
toolbox-meatball-menu-community-label = Topluluk…
# This menu item is only available in the browser toolbox. It forces the popups/panels
# to stay visible on blur, which is primarily useful for addon developers and Firefox
# contributors.
toolbox-meatball-menu-noautohide-label = Açılır pencerelerin otomatik gizlenmesini kapat
toolbox-meatball-menu-pseudo-locale-accented = “Aksanlı” dili etkinleştir
toolbox-meatball-menu-pseudo-locale-bidi = “Sağdan sola” dili etkinleştir

##

