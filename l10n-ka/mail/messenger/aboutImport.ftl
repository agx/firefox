# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = გადმოტანა

## Header

import-from-app = გადმოტანა პროგრამიდან
import-from-app-desc = აირჩიეთ ანგარიშების, წიგნაკების, კალენდრებისა და სხვა მონაცემების გადმოსატანად:
import-address-book = წიგნაკის ფაილის გადმოტანა
import-calendar = კალენდრის ფაილის გადმოტანა

## Buttons

button-cancel = გაუქმება
button-back = უკან
button-continue = განაგრძეთ

## Import from app steps

# Variables:
#   $app (String) - The name of the app to import from
profiles-pane-title = გადმოტანა – { $app }
profiles-pane-desc = მიუთითეთ მდებარეობა, საიდან უნდა გადმოვიდეს
profile-file-picker-dir = აირჩიეთ პროფილის საქაღალდე
profile-file-picker-zip = აირჩიეთ zip-ფაილი (არაუმეტეს 2გბ)
items-pane-title = გადმოსატანის არჩევა
items-pane-desc = საიდან გადმოვიდეს
items-pane-checkbox-accounts = ანგარიშები და პარამეტრები
items-pane-checkbox-address-books = წიგნაკები
items-pane-checkbox-calendars = კალენდრები
items-pane-checkbox-mail-messages = ფოსტის წერილები

## Import dialog

progress-pane-title = გადმოდის
progress-pane-restart-desc = ახლიდან გაუშვით, დასასრულებლად.
error-pane-title = შეცდომა
error-message-zip-file-too-big = შერჩეული zip-ფაილი აღემატება 2GB-ს. გთხოვთ ჯერ დაშალოთ და შემდეგ საქაღალდით შემოიტანოთ.
error-message-extract-zip-file-failed = ვერ მოხერხდა zip-ფაილის გაშლა. გთხოვთ, ხელით ამოიღოთ შიგთავსი და იმ საქაღალდიდან შემოიტანოთ.
error-message-failed = შემოტანა მოულოდნელად შეფერხდა, ვრცლად იხილავთ შეცდომების კონსოლში.
