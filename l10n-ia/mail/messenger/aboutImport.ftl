# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = Importar

## Header

import-from-app = Importar ab application
import-from-app-desc = Seliger de importar contos, libros de adresses, agendas, e altere dato ab:
import-address-book = Importar le file libro del adresses
import-calendar = Importar file de agenda

## Buttons

button-cancel = Cancellar
button-back = Retro
button-continue = Continuar

## Import from app steps

# Variables:
#   $app (String) - The name of the app to import from
profiles-pane-title = Importar ab { $app }
profiles-pane-desc = Selige le position ab le qual importar.
profile-file-picker-dir = Selige un plica del profilo
profile-file-picker-zip = Selige un file zip (minor de 2GB)
items-pane-title = Elige que importar
items-pane-desc = Importar ab
items-pane-checkbox-accounts = Contos e parametros
items-pane-checkbox-address-books = Libros de adresses
items-pane-checkbox-calendars = Agendas
items-pane-checkbox-mail-messages = Messages e-mail

## Import dialog

progress-pane-title = Importation
progress-pane-restart-desc = Reinitiar pro finir de importar.
error-pane-title = Error
error-message-zip-file-too-big = Le file zip seligite, es major de 2GB. In vice abstrahe lo antea primo, postea importa lo ab le plicas extrahite.
error-message-extract-zip-file-failed = Impossibile extraher le file zip. In vice extrahe lo manualmente, pois importa lo ab le plica extrahite.
error-message-failed = Le importation falleva inspectatemente, Altere informationes pote esser disponibile in le Consola de error.
