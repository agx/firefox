# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = Reinitio necessari
restart-required-header = Excusa le interruption: il resta solmente un micre cosa a facer pro poter continuar.
restart-required-intro-brand = { -brand-short-name } ha justo essite actualisate in secunde plano. Clicca sur Reinitiar { -brand-short-name } pro completar le actualisation.
restart-required-description = Nos va restaurar tote tu paginas, fenestras e schedas. Tu potera immediatemente reprender tu activitates.
restart-required-heading = Reinitiar pro continuar a usar { -brand-short-name }
restart-required-intro = Un actualisation a { -brand-short-name } comenciava in secunde plano. Tu debera reinitiar pro finir le actualisation.
window-restoration-info = Tu fenestras e schedas essera rapidemente restaurate, ma non le privates.
restart-button-label = Reinitiar { -brand-short-name }
