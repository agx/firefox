# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = Импорттау

## Header

import-from-app = Қолданбадан импорттау

## Buttons

button-cancel = Бас тарту
button-back = Артқа
button-continue = Жалғастыру

## Import from app steps

items-pane-checkbox-calendars = Күнтізбелер
items-pane-checkbox-mail-messages = Пошта хабарламалары

## Import dialog

progress-pane-title = Импорттау
progress-pane-restart-desc = Импорттауды аяқтау үшін қайта іске қосыңыз.
error-pane-title = Қате
