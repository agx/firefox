# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = Қайта іске қосу керек
restart-required-header = Кешіріңіз. Жалғастыру үшін кішкентай нәрсені жасауымыз керек.
restart-required-intro-brand = { -brand-short-name } фондық режимде жаңартылды. Жаңартуды аяқтау үшін { -brand-short-name } қайта іске қосу батырмасын басыңыз.
restart-required-description = Жұмысыңызды тежемеу үшін, біз барлық беттер және терезелерді қалпына келтіреміз.
restart-required-heading = { -brand-short-name } қолдануды жалғастыру үшін қайта қосу
restart-required-intro = { -brand-short-name } жаңартуы фондық режимде басталды. Жаңартуды аяқтау үшін қайта іске қосу керек.
window-restoration-info = Жекелік терезелерден басқа терезелер мен беттер жылдам қалпына келтіріледі.
restart-button-label = { -brand-short-name } қайта қосу
