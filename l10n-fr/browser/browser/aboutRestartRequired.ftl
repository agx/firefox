# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = Redémarrage nécessaire
restart-required-header = Pardon du dérangement, il reste un dernier détail à régler avant de continuer.
restart-required-intro-brand = { -brand-short-name } vient d’être mis à jour en arrière-plan. Cliquez sur Redémarrer { -brand-short-name } pour terminer la mise à jour.
restart-required-description = Toutes vos pages, fenêtres et onglets seront restaurés pour que vous puissiez reprendre rapidement.
restart-required-heading = Redémarrez pour continuer à utiliser { -brand-short-name }
restart-required-intro = Une mise à jour de { -brand-short-name } a démarré en arrière-plan. Vous devrez redémarrer pour terminer la mise à jour.
window-restoration-info = Vos fenêtres et onglets seront rapidement restaurés, sauf les privés.
restart-button-label = Redémarrer { -brand-short-name }
