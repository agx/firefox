# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = Mewnforio

## Header

import-from-app = Mewnforio o'r Rhaglen
import-from-app-desc = Dewis i fewnforio Cyfrifon, Llyfrau Cyfeiriadau, Calendrau a data o:
import-address-book = Mewnforio Ffeil Llyfr Cyfeiriadau
import-calendar = Mewnforio Ffeil Calendr

## Buttons

button-cancel = Diddymu
button-back = Nôl
button-continue = Parhau

## Import from app steps

# Variables:
#   $app (String) - The name of the app to import from
profiles-pane-title = Mewnforio o { $app }
profiles-pane-desc = Dewiswch y lleoliad i fewnforio ohono
profile-file-picker-dir = Dewiswch ffolder proffil
profile-file-picker-zip = Dewiswch ffeil zip (llai na 2GB)
items-pane-title = Dewiswch beth i'w fewnforio
items-pane-desc = Mewnforio o
items-pane-checkbox-accounts = Cyfrifon a Gosodiadau
items-pane-checkbox-address-books = Llyfrau Cyfeiriadau
items-pane-checkbox-calendars = Calendrau
items-pane-checkbox-mail-messages = Negeseuon E-bost

## Import dialog

progress-pane-title = Yn mewnforio
progress-pane-restart-desc = Ailgychwyn i orffen mewnforio.
error-pane-title = Gwall
error-message-zip-file-too-big = Mae'r ffeil zip a ddewiswyd yn fwy na 2GB. Echdynnwch hi'n gyntaf, yna ei mewnforio o'r ffolder cafodd ei hechdynnu.
error-message-extract-zip-file-failed = Wedi methu echdynnu'r ffeil zip. Echdynwch hi â llaw, yna ei mewnforio o'r ffolder wedi'i hechdynnu yn lle hynny.
error-message-failed = Methodd y mewnforio yn annisgwyl, efallai y bydd mwy o wybodaeth ar gael yn y Consol Gwallau.
