# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = Mae Angen Ail Gychwyn
restart-required-header = Ymddiheuriadau. Mae angen gwneud un peth bach er mwyn cadw i fynd.
restart-required-intro-brand = Mae { -brand-short-name } newydd gael ei ddiweddaru yn y cefndir. Cliciwch Ailgychwyn { -brand-short-name } i gwblhau'r diweddariad.
restart-required-description = Byddwn yn adfer eich holl dudalennau, ffenestri a thabiau wedyn , fel bod modd i chi fod ar eich ffordd yn sydyn.
restart-required-heading = Ailgychwyn i Barhau i Ddefnyddio { -brand-short-name }
restart-required-intro = Dechreuwyd diweddaru { -brand-short-name } yn y cefndir. Bydd angen i chi ailgychwyn i orffen y diweddariad.
window-restoration-info = Bydd eich ffenestri a'ch tabiau'n cael eu hadfer yn gyflym, ond nid y rhai preifat.
restart-button-label = Ailgychwyn { -brand-short-name }
