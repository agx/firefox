# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### These messages are used in the DevTools toolbox.


## These labels are shown in the "..." menu in the toolbox, and represent different
## commands such as the docking of DevTools, toggling features, and viewing some
## external links. Some of the commands have the keyboard shortcut shown next to
## the label.

toolbox-meatball-menu-dock-bottom-label = Docio i'r gwaelod
toolbox-meatball-menu-dock-left-label = Docio i'r chwith
toolbox-meatball-menu-dock-right-label = Docio i'r dde
toolbox-meatball-menu-dock-separate-window-label = Ffenestr ar wahân
toolbox-meatball-menu-splitconsole-label = Dangos consol hollt
toolbox-meatball-menu-hideconsole-label = Cuddio'r consol hollt
toolbox-meatball-menu-settings-label = Gosodiadau
toolbox-meatball-menu-documentation-label = Dogfennaeth…
toolbox-meatball-menu-community-label = Cymuned…
# This menu item is only available in the browser toolbox. It forces the popups/panels
# to stay visible on blur, which is primarily useful for addon developers and Firefox
# contributors.
toolbox-meatball-menu-noautohide-label = Analluogi awtoguddio llamlenni
toolbox-meatball-menu-pseudo-locale-accented = Galluogi locale “acennog”.
toolbox-meatball-menu-pseudo-locale-bidi = Galluogi locale “bidi”.

##

