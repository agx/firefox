# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## These strings are used in the about:preferences moreFromMozilla page

more-from-moz-title = Autres produits de { -vendor-short-name }
more-from-moz-category =
    .tooltiptext = Autres produits de { -vendor-short-name }
more-from-moz-firefox-mobile-title = { -brand-product-name } mobil
more-from-moz-firefox-mobile-description = Lo navegador mobil que fa passar la confidencialitat d’en primièr.
more-from-moz-mozilla-vpn-title = { -mozilla-vpn-brand-name }
# This string is specific to the product Mozilla Rally which is US only.
more-from-moz-mozilla-rally-title = { -rally-brand-name }
# This string is specific to the product Mozilla Rally which is US only.
more-from-moz-button-mozilla-rally =
    .label = Rejónher { -rally-short-name }
# This string is specific to the product Mozilla Rally which is US only.
more-from-moz-button-mozilla-rally-2 = Rejónher { -rally-short-name }
more-from-moz-qr-code-box-firefox-mobile-button = Enviatz un e-mail a vòstre telefòn a la plaça
more-from-moz-button-mozilla-vpn =
    .label = Obténer lo VPN
more-from-moz-qr-code-firefox-mobile-img =
    .alt = Còdi QR per telecargar { -brand-product-name } mobile
more-from-moz-button-mozilla-vpn-2 = Obténer lo VPN
more-from-moz-learn-more-link = Ne saber mai
