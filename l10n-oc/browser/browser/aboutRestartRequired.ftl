# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = Reaviada necessària
restart-required-header = O planhèm, manca pas gaire per poder contunhar.
restart-required-intro-brand = { -brand-short-name } s’es mes a jorn en rèireplan. Clicatz Reaviar { -brand-short-name } per terminar la mesa a jorn.
restart-required-description = Restablirem totas las paginas, fenèstras e onglets, per que poscatz rapidament tornar ont èretz.
restart-required-heading = Reaviatz per téner d’utilizar { -brand-short-name }
restart-required-intro = Una mesa a jorn de { -brand-short-name } a començat en rèireplan. Vos calrà reaviar per terminar la mesa a jorn.
window-restoration-info = Vòstras fenèstras e onglets seràn rapidament restablits, çò privat o serà pas.
restart-button-label = Reavir { -brand-short-name }
