# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = إعادة التشغيل مطلوبة
restart-required-header = نأسف لهذا، لكن نحتاج لإجراء آخر بسيط للمواصلة.
restart-required-intro-brand = اكتمل تحديث { -brand-short-name } للتو وأنت تعمل. انقر ”أعِد تشغيل { -brand-short-name }“ لإكمال التحديث.
restart-required-description = سنستعيد كل الصفحات والنوافذ والألسنة بعد ذلك حتى يمكنك العودة إلى ما كنت فيه بسرعة.
restart-required-heading = أعِد التشغيل لمواصلة استعمال { -brand-short-name }
restart-required-intro = بدأ تحديث { -brand-short-name } في الخلفية. عليك إعادة تشغيل التطبيق حين ينتهي التحديث، لإكمال العملية.
window-restoration-info = ستُستعاد النوافذ والألسنة بسرعة، ولكنك ستفقد الخاصة منها.
restart-button-label = أعِد تشغيل { -brand-short-name }
