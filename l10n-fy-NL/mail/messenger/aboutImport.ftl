# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = Ymportearje

## Header

import-from-app = Ymportearje út tapassing
import-from-app-desc = Kies oft jo accounts, adresboeken, aginda’s en oare gegevens ymportearje wolle út:
import-address-book = Adresboekbestân ymportearje
import-calendar = Agindabestân ymportearje

## Buttons

button-cancel = Annulearje
button-back = Tebek
button-continue = Trochgean

## Import from app steps

# Variables:
#   $app (String) - The name of the app to import from
profiles-pane-title = Ymportearje út { $app }
profiles-pane-desc = Kies de lokaasje wêrfan jo ymportearje wolle
profile-file-picker-dir = Selektearje in profylmap
profile-file-picker-zip = Selektearje in zip-bestân (lytser as 2GB)
items-pane-title = Selektearje wat jo ymportearje wolle
items-pane-desc = Ymportearje út
items-pane-checkbox-accounts = Accounts en ynstellingen
items-pane-checkbox-address-books = Adresboeken
items-pane-checkbox-calendars = Aginda’s
items-pane-checkbox-mail-messages = E-mailberjochten

## Import dialog

progress-pane-title = Ymportearje
progress-pane-restart-desc = Opnij starte om it ymportearjen te foltôgjen.
error-pane-title = Flater
error-message-zip-file-too-big = It selektearre zip-bestân is grutter as 2 GB. Pak it earst út en ymportearje it dernei út de útpakte map.
error-message-extract-zip-file-failed = Kin it zipbestân net útpakke. Pak it hânmjittich út en ymportearje it dernei út de útpakte map.
error-message-failed = Ymportearjen is ûnferwachte mislearre, mear ynformaasje is mooglik beskikber yn de Flaterconsole.
