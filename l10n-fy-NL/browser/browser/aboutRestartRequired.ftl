# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = Opnij starte fereaske
restart-required-header = Sorry. Wy moatte noch ien lyts ding dwaan om troch te gean.
restart-required-intro-brand = { -brand-short-name } is sakrekt op de eftergrûn bywurke. Klik op { -brand-short-name } opnij starte om de fernijing te foltôgjen.
restart-required-description = Dêrnei wurde al jo siden, finsters en ljepblêden wer wersteld, sadat jo fluch fierder kinne.
restart-required-heading = Start { -brand-short-name } opnij om brûke te bliuwen
restart-required-intro = In fernijing foar { -brand-short-name } is op de eftergrûn start. Jo moatte opnij opstarte om de fernijing te foltôgjen.
window-restoration-info = Jo finsters en ljepblêden wurde fluch wersteld, mar priveefinsters en -ljepblêden net.
restart-button-label = { -brand-short-name } opnij starte
