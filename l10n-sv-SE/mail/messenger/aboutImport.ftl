# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = Importera

## Header

import-from-app = Importera från applikation
import-from-app-desc = Välj att importera konton, adressböcker, kalendrar och annan data från:
import-address-book = Importera adressboksfil
import-calendar = Importera kalenderfil

## Buttons

button-cancel = Avbryt
button-back = Tillbaka
button-continue = Fortsätt

## Import from app steps

# Variables:
#   $app (String) - The name of the app to import from
profiles-pane-title = Importera från { $app }
profiles-pane-desc = Välj varifrån du vill importera
profile-file-picker-dir = Välj en profilmapp
profile-file-picker-zip = Välj en zip-fil (mindre än 2 GB)
items-pane-title = Välj vad som ska importeras
items-pane-desc = Importera från
items-pane-checkbox-accounts = Konton och inställningar
items-pane-checkbox-address-books = Adressböcker
items-pane-checkbox-calendars = Kalendrar
items-pane-checkbox-mail-messages = E-postmeddelanden

## Import dialog

progress-pane-title = Importerar
progress-pane-restart-desc = Starta om för att slutföra importen.
error-pane-title = Fel
error-message-zip-file-too-big = Den valda zip-filen är större än 2 GB. Extrahera det först och importera sedan från den extraherade mappen istället.
error-message-extract-zip-file-failed = Det gick inte att extrahera zip-filen. Extrahera den manuellt och importera sedan från den extraherade mappen istället.
error-message-failed = Importen misslyckades oväntat, mer information kan finnas tillgänglig i felkonsolen.
