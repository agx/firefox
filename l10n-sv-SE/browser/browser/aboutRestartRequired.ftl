# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = Omstart krävs
restart-required-header = Förlåt. Vi behöver bara göra en liten sak för att fortsätta.
restart-required-intro-brand = { -brand-short-name } har precis uppdaterats i bakgrunden. Klicka på starta om { -brand-short-name } för att slutföra uppdateringen.
restart-required-description = Vi kommer att återställa alla dina sidor, fönster och flikar efteråt, så att du snabbt kan fortsätta.
restart-required-heading = Starta om för att fortsätta använda { -brand-short-name }
restart-required-intro = En uppdatering av { -brand-short-name } startade i bakgrunden. Du måste starta om för att slutföra uppdateringen.
window-restoration-info = Dina fönster och flikar kommer snabbt att återställas, men privata kommer inte att göra det.
restart-button-label = Starta om { -brand-short-name }
