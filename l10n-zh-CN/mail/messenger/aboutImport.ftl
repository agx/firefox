# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = 导入

## Header

import-from-app = 从应用程序导入
import-from-app-desc = 选择从下列位置导入账户、通讯录、日历和其他数据：
import-address-book = 导入通讯录文件
import-calendar = 导入日历文件

## Buttons

button-cancel = 取消
button-back = 上一步
button-continue = 继续

## Import from app steps

# Variables:
#   $app (String) - The name of the app to import from
profiles-pane-title = 从 { $app } 导入
profiles-pane-desc = 请选择要从何处导入数据
profile-file-picker-dir = 选择配置文件夹
profile-file-picker-zip = 选择 ZIP 文件（小于 2GB）
items-pane-title = 选择要导入的项目
items-pane-desc = 从下列位置导入
items-pane-checkbox-accounts = 账户和设置
items-pane-checkbox-address-books = 通讯录
items-pane-checkbox-calendars = 日历
items-pane-checkbox-mail-messages = 邮件消息

## Import dialog

progress-pane-title = 正在导入…
progress-pane-restart-desc = 重启客户端以完成导入。
error-pane-title = 错误
error-message-zip-file-too-big = 所选的 ZIP 文件大于 2GB。请先解压缩，然后从解压缩的文件夹中导入。
error-message-extract-zip-file-failed = 无法解压缩 ZIP 文件。请手动解压缩，并导入解压缩后的文件夹。
error-message-failed = 导入意外失败，错误控制台中可能提供有更多信息。
