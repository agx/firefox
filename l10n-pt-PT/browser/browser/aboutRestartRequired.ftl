# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = Reinício necessário
restart-required-header = Desculpe. Precisamos de fazer uma pequena coisa para continuar.
restart-required-intro-brand = O { -brand-short-name } foi atualizado em segundo plano. Clique em Reiniciar o { -brand-short-name } para concluir a atualização.
restart-required-description = Iremos restaurar todas as suas páginas, janelas e separadores depois, para que possa estar no seu caminho rapidamente.
restart-required-heading = Reiniciar para continuar a utilizar o { -brand-short-name }
restart-required-intro = Começou uma atualização em segundo plano para o { -brand-short-name }. Terá de reiniciar para concluir a atualização.
window-restoration-info = As suas janelas e separadores serão rapidamente restaurados, com exceção dos privados.
restart-button-label = Reiniciar o { -brand-short-name }
