# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = Herstart vereist
restart-required-header = Sorry. We moeten nog een klein dingetje uitvoeren om te kunnen blijven werken.
restart-required-intro-brand = { -brand-short-name } is zojuist op de achtergrond bijgewerkt. Klik op { -brand-short-name } herstarten om de update te voltooien.
restart-required-description = Daarna worden al uw pagina’s, vensters en tabbladen weer hersteld, zodat u snel verder kunt.
restart-required-heading = Herstarten om { -brand-short-name } te blijven gebruiken
restart-required-intro = Een update voor { -brand-short-name } is op de achtergrond gestart. U dient opnieuw op te starten om de update te voltooien.
window-restoration-info = Uw vensters en tabbladen worden snel hersteld, maar privévensters en -tabbladen niet.
restart-button-label = { -brand-short-name } herstarten
