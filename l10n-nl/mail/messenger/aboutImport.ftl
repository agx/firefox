# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = Importeren

## Header

import-from-app = Importeren uit toepassing
import-from-app-desc = Kies of u accounts, adresboeken, agenda’s en andere gegevens wilt importeren uit:
import-address-book = Adresboekbestand importeren
import-calendar = Agendabestand importeren

## Buttons

button-cancel = Annuleren
button-back = Terug
button-continue = Doorgaan

## Import from app steps

# Variables:
#   $app (String) - The name of the app to import from
profiles-pane-title = Importeren uit { $app }
profiles-pane-desc = Kies de locatie van waaruit u wilt importeren
profile-file-picker-dir = Selecteer een profielmap
profile-file-picker-zip = Selecteer een zipbestand (kleiner dan 2GB)
items-pane-title = Selecteer wat u wilt importeren
items-pane-desc = Importeren uit
items-pane-checkbox-accounts = Accounts en instellingen
items-pane-checkbox-address-books = Adresboeken
items-pane-checkbox-calendars = Agenda’s
items-pane-checkbox-mail-messages = E-mailberichten

## Import dialog

progress-pane-title = Importeren
progress-pane-restart-desc = Herstart om het importeren te voltooien.
error-pane-title = Fout
error-message-zip-file-too-big = Het geselecteerde zipbestand is groter dan 2 GB. Pak het eerst uit en importeer het vervolgens uit de uitgepakte map.
error-message-extract-zip-file-failed = Kan het zipbestand niet uitpakken. Pak het handmatig uit en importeer het vervolgens uit de uitgepakte map.
error-message-failed = Importeren is onverwacht mislukt, meer informatie is mogelijk beschikbaar in de Foutconsole.
