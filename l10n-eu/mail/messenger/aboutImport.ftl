# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = Inportatu

## Header

import-from-app = Inportatu aplikaziotik
import-from-app-desc = Aukeratu inportatzeko Kontuak, Helbide-liburuak, Egutegiak eta bestelako datuak hemendik:
import-address-book = Inportatu helbide-liburu fitxategia
import-calendar = Inportatu egutegi fitxategia

## Buttons

button-cancel = Utzi
button-back = Atzera
button-continue = Jarraitu

## Import from app steps

# Variables:
#   $app (String) - The name of the app to import from
profiles-pane-title = Inportatu hemendik { $app }
profiles-pane-desc = Aukeratu inportazio kokapena
profile-file-picker-dir = Aukeratu profil karpeta
profile-file-picker-zip = Aukeratu zip fitxategia (2GB baino txikiagoa)
items-pane-title = Aukeratu zer inportatu
items-pane-desc = Inportatu hemendik
items-pane-checkbox-accounts = Kontu eta ezarpenak
items-pane-checkbox-address-books = Helbide-liburuak
items-pane-checkbox-calendars = Egutegiak
items-pane-checkbox-mail-messages = Posta-mezuak

## Import dialog

progress-pane-title = Inportatzen
progress-pane-restart-desc = Berrabiarazi inportazioa amaitzeko.
error-pane-title = Errorea
error-message-zip-file-too-big = Hautatutako Zip fitxategia 2GB baino handiagoa da. Mesedez, erauzi lehenengo eta gero inportatu erauzitako karpeta.
error-message-extract-zip-file-failed = Huts egin du zip fitxategia erauzteak. Mesedez erauzi eskuz, ondoren inportatu erauzitako karpeta.
error-message-failed = Huts egin du inportazioak ustekabean, informazio gehiago eskuragarri errore kontsolan.
