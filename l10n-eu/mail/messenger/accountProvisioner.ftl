# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

account-provisioner-tab-title = Lortu posta elektroniko helbide berria zerbitzu hornitzaile batengandik
provisioner-searching-icon =
    .alt = Bilatzen…
account-provisioner-title = Sortu posta elektroniko berri bat
account-provisioner-mail-account-title = Erosi posta elektroniko berri bat
account-provisioner-domain-title = Erosi posta helbidea eta domeinua zuretzat

## Forms

account-provisioner-search-button = Bilatu
account-provisioner-button-cancel = Utzi
account-provisioner-button-existing = Ezarri baduzun posta elektroniko kontu bat
account-provisioner-button-back = Joan atzera

## Notifications


## Illustrations


## Search results

account-provisioner-free-account = Doan
account-provision-price-per-year = { $price } urtean
account-provisioner-all-results-button = Erakutsi emaitza guztiak
account-provisioner-open-in-tab-img =
    .title = Ireki fitxa berri batean
