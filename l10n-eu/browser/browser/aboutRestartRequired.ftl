# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = Berrabiarazi egin behar da
restart-required-header = Barkatu, gauzatxo bat egin behar dugu jarraitu ahal izateko.
restart-required-intro-brand = { -brand-short-name } atzeko planoan eguneratu berri da. Egin klik 'Berrabiarazi { -brand-short-name }' botoian eguneraketa burutzeko.
restart-required-description = Zure orri, leiho eta fitxa guztiak berreskuratuko ditugu gero, ari zinenarekin azkar jarraitu ahal dezazun.
restart-required-heading = Berrabiarazi { -brand-short-name } erabiltzen jarraitzeko
restart-required-intro = { -brand-short-name }(r)en eguneraketa bat hasi da atzeko planoan. Berrabiarazi egin beharko duzu eguneratzen amaitzeko.
window-restoration-info = Zure leihoak eta fitxak laster batean berreskuratuko dira; leiho edo fitxa pribaturik ez da berreskuratuko ordea.
restart-button-label = Berrabiarazi { -brand-short-name }
