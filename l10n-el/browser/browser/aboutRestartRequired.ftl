# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = Απαιτείται επανεκκίνηση
restart-required-header = Συγγνώμη. Θα πρέπει να κάνουμε μια μικρή εργασία για να συνεχίσουμε.
restart-required-intro-brand = Το { -brand-short-name } μόλις ενημερώθηκε στο παρασκήνιο. Κάντε κλικ στο "Επανεκκίνηση του { -brand-short-name }" για να ολοκληρωθεί η ενημέρωση.
restart-required-description = Θα επαναφέρουμε όλες τις σελίδες, τα παράθυρα και τις καρτέλες σας αμέσως μετά. Έτσι, θα μπορέσετε να συνεχίσετε γρήγορα.
restart-required-heading = Απαιτείται επανεκκίνηση για χρήση του { -brand-short-name }
restart-required-intro = Μια ενημέρωση του { -brand-short-name } ξεκίνησε στο παρασκήνιο. Θα χρειαστεί να κάνετε επανεκκίνηση προκειμένου να ολοκληρωθεί.
window-restoration-info = Τα παράθυρα και οι καρτέλες σας θα ανακτηθούν γρήγορα, με εξαίρεση τα ιδιωτικά.
restart-button-label = Επανεκκίνηση του { -brand-short-name }
