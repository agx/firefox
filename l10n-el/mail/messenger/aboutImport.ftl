# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = Εισαγωγή

## Header

import-from-app = Εισαγωγή από εφαρμογή
import-from-app-desc = Επιλέξτε για εισαγωγή Λογαριασμών, Βιβλίων διευθύνσεων, Ημερολογίων και άλλων δεδομένων από:
import-address-book = Εισαγωγή αρχείου ευρετηρίου
import-calendar = Εισαγωγή αρχείου ημερολογίου

## Buttons

button-cancel = Ακύρωση
button-back = Πίσω
button-continue = Συνέχεια

## Import from app steps

# Variables:
#   $app (String) - The name of the app to import from
profiles-pane-title = Εισαγωγή από { $app }
profiles-pane-desc = Επιλέξτε την τοποθεσία από την οποία θα γίνει εισαγωγή
profile-file-picker-dir = Επιλέξτε ένα φάκελο προφίλ
profile-file-picker-zip = Επιλέξτε ένα αρχείο zip (μικρότερο από 2 GB)
items-pane-title = Επιλέξτε τι θα εισαγάγετε
items-pane-desc = Εισαγωγή από
items-pane-checkbox-accounts = Λογαριασμοί και ρυθμίσεις
items-pane-checkbox-address-books = Ευρετήρια
items-pane-checkbox-calendars = Ημερολόγια
items-pane-checkbox-mail-messages = Μηνύματα email

## Import dialog

progress-pane-title = Εισαγωγή
progress-pane-restart-desc = Κάντε επανεκκίνηση για να ολοκληρώσετε την εισαγωγή.
error-pane-title = Σφάλμα
error-message-zip-file-too-big = Το επιλεγμένο αρχείο zip είναι μεγαλύτερο από 2 GB. Παρακαλώ εξαγάγετέ το πρώτα και έπειτα, κάντε εισαγωγή από τον φάκελο εξαγωγής.
error-message-extract-zip-file-failed = Αποτυχία εξαγωγής του αρχείου zip. Εξαγάγετέ το με μη αυτόματο τρόπο και στη συνέχεια πραγματοποιήστε εισαγωγή από τον φάκελο εξαγωγής.
error-message-failed = Η εισαγωγή απέτυχε απροσδόκητα, περισσότερες πληροφορίες ενδέχεται να είναι διαθέσιμες στην Κονσόλα Σφαλμάτων.
