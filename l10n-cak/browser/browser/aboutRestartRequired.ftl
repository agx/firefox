# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = K'o chi Nitikirisäx
restart-required-header = Takuyu'. Xa xe k'o chi niqab'än jun ko'öl wachinäq richin yojsamäj chik el.
restart-required-intro-brand = { -brand-short-name } xuk'ëx ri' pa ruka'n k'ojlib'äl. Tapitz'a' pa Titikirisäx chik { -brand-short-name } richin natz'aqatisaj ri k'exoj.
restart-required-description = Xkeqak'ojoj ronojel ri taq ruxaq ak'amaya'l, taq atzuwäch chuqa' taq awi', richin anin natikirisaj chik el.
restart-required-heading = Tatikirisaj chik richin k'a nawokisaj { -brand-short-name }
restart-required-intro = Xtikirisäx pa jun ruka'n b'ey jun ruk'exoj { -brand-short-name }. K'o chi natikirisaj chik richin nak'ïs ri k'exoj.
window-restoration-info = Anin xkek'oje' pa rub'eyal ri taq atzuwäch chuqa' ri taq ruwi', po man ke ta ri' ri e'ichinan.
restart-button-label = Titikirisäx chik { -brand-short-name }
