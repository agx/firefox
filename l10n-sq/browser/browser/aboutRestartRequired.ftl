# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = Lyp Rinisje
restart-required-header = Na ndjeni. Na duhet të bëjmë një gjë të vockël për të vazhduar.
restart-required-intro-brand = { -brand-short-name } sapo u përditësua në prapaskenë. Klikoni mbi Rinise { -brand-short-name } që të plotësohet përditësimi.
restart-required-description = Më pas do të rikthejmë krejt faqet, dritaret dhe skedat tuaja, që të jeni gati pa humbur kohë.
restart-required-heading = Që të Vazhdoni të Përdorni { -brand-short-name }, riniseni
restart-required-intro = Ka filluar një përditësim i { -brand-short-name } në prapaskenë. Që të përfundojë përditësimi, do të duhet ta rinisni.
window-restoration-info = Dritaret dhe skedat tuaja do të rikthehen në çast, por jo ato private ama.
restart-button-label = Rinise { -brand-short-name }-in
