# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = Importim

## Header

import-from-app = Importo prej Aplikacioni
import-from-app-desc = Zgjidhni të importohet nga Llogari, Libra Adresash dhe të dhëna të tjera nga:
import-address-book = Importo Kartelë Libri Adresash
import-calendar = Importo Kartelë Kalendari

## Buttons

button-cancel = Anuloje
button-back = Mbrapsht
button-continue = Vazhdo

## Import from app steps

# Variables:
#   $app (String) - The name of the app to import from
profiles-pane-title = Importo nga { $app }
profiles-pane-desc = Zgjidhni vendndodhjen prej nga të importohet
profile-file-picker-dir = Përzgjidhni një dosje profili
profile-file-picker-zip = Përzgjidhni një kartelë zip (më të vogël se 2GB)
items-pane-title = Përzgjidhni ç’të importohet
items-pane-desc = Importo nga
items-pane-checkbox-accounts = Llogari dhe Rregullime
items-pane-checkbox-address-books = Libra Adresash
items-pane-checkbox-calendars = Kalendarë
items-pane-checkbox-mail-messages = Mesazhe Poste

## Import dialog

progress-pane-title = Importim
progress-pane-restart-desc = Riniseni, që të përfundohet importimi.
error-pane-title = Gabim
error-message-zip-file-too-big = Kartela zip e përzgjedhur është më e madhe se 2GB. Ju lutemi, së pari çngjesheni, mandej bëni importim prej dosjes së përftuar.
error-message-extract-zip-file-failed = S’u arrit të përftohet kartela zip. Ju lutemi, përftojeni dorazi, mandej importojeni që nga dosja e përftuar.
error-message-failed = Importimi dështoi papritmas, më tepër hollësi mund të ketë te Konsola e Gabimeve.
