# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = Importovanie

## Header

import-from-app = Import z aplikácie
import-from-app-desc = Zvoľte, odkiaľ chcete importovať účty, adresáre, kalendáre a ďalšie údaje:
import-address-book = Import súboru adresára
import-calendar = Import súboru s kalendárom

## Buttons

button-cancel = Zrušiť
button-back = Naspäť
button-continue = Pokračovať

## Import from app steps

# Variables:
#   $app (String) - The name of the app to import from
profiles-pane-title = Import z aplikácie { $app }
profiles-pane-desc = Vyberte umiestnenie, z ktorého chcete importovať
profile-file-picker-dir = Vyberte priečinok profilu
profile-file-picker-zip = Vyberte súbor zip (menší ako 2 GB)
items-pane-title = Zvoľte, čo chcete importovať
items-pane-desc = Importovať z
items-pane-checkbox-accounts = Účty a nastavenia
items-pane-checkbox-address-books = Adresáre
items-pane-checkbox-calendars = Kalendáre
items-pane-checkbox-mail-messages = E-mailové správy

## Import dialog

progress-pane-title = Importuje sa
progress-pane-restart-desc = Reštartovaním dokončíte import.
error-pane-title = Chyba
error-message-zip-file-too-big = Zvolený súbor zip je väčší ako 2 GB. Najprv ho rozbaľte a potom importujte z extrahovaného priečinka.
error-message-extract-zip-file-failed = Nepodarilo sa extrahovať súbor zip. Rozbaľte ho ručne a potom ho importujte z extrahovaného priečinka.
error-message-failed = Import neočakávane zlyhal, ďalšie informácie môžu byť k dispozícii v Chybovej konzole.
