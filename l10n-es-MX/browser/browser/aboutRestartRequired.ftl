# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = Se necesita reiniciar
restart-required-header = Lo sentimos. Solo necesitamos hacer un pequeña cosa para continuar.
restart-required-intro-brand = { -brand-short-name } ha sido actualizado en segundo plano. Haz clic en reiniciar { -brand-short-name } para completar la actualización.
restart-required-description = Restauraremos todas tus páginas, ventanas y pestañas enseguida, para que puedas continuar rápidamente.
restart-required-heading = Reiniciar para continuar usando { -brand-short-name }
restart-required-intro = Se inició una actualización de { -brand-short-name } en segundo plano. Tendrás que reiniciar para finalizar la actualización.
window-restoration-info = Tus ventanas y pestañas se restaurarán rápidamente, pero no las privadas.
restart-button-label = Reiniciar { -brand-short-name }
