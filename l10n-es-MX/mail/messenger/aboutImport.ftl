# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = Importar

## Header

import-from-app = Importar desde la aplicación
import-from-app-desc = Elige importar cuentas, libretas de direcciones, calendarios y otros datos de:
import-address-book = Importar archivo de libreta de direcciones
import-calendar = Importar archivo de calendario

## Buttons

button-cancel = Cancelar
button-back = Atrás
button-continue = Continuar

## Import from app steps

# Variables:
#   $app (String) - The name of the app to import from
profiles-pane-title = Importar desde { $app }
profiles-pane-desc = Elegir la ubicación desde la cual importar
profile-file-picker-dir = Seleccionar una carpeta de perfil
profile-file-picker-zip = Seleccionar un archivo zip (menor a 2GB)
items-pane-title = Seleccionar que importar
items-pane-desc = Importar desde
items-pane-checkbox-accounts = Cuentas y configuración
items-pane-checkbox-address-books = Libretas de direcciones
items-pane-checkbox-calendars = Calendarios
items-pane-checkbox-mail-messages = Mensajes de correo

## Import dialog

progress-pane-title = Importando
progress-pane-restart-desc = Reiniciar para terminar de importar.
error-pane-title = Error
error-message-zip-file-too-big = El archivo zip seleccionado tiene un tamaño superior a 2GB. Por favor, extraelo primero y luego impórtalo de la carpeta extraída.
error-message-extract-zip-file-failed = No se pudo extraer el archivo zip. Por favor, extraerlo manualmente y luego importarlo desde la carpeta extraída.
error-message-failed = La importación falló inesperadamente, es posible que haya más información disponible en la consola de errores.
