# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = Genstart påkrævet
restart-required-header = Du bliver nødt til at genstarte din browser.
restart-required-intro-brand = { -brand-short-name } er blevet opdateret i baggrunden. Klik på Genstart { -brand-short-name } for at gøre opdateringen færdig.
restart-required-description = Vi genskaber alle dine åbne sider, vinduer og faneblade, så du hurtigt kan komme videre.
restart-required-heading = Genstart for at blive ved med at bruge { -brand-short-name }
restart-required-intro = En opdatering af { -brand-short-name } blev startet i baggrunden. Du skal genstarte for at gennemføre opdateringen.
window-restoration-info = Dine vinduer og faneblade vil blive genskabt, bortset fra de private.
restart-button-label = Genstart { -brand-short-name }
