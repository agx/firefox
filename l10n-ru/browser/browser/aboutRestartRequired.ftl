# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = Требуется перезапуск
restart-required-header = Извините. Чтобы продолжить работу, нам нужно сделать одну небольшую вещь.
restart-required-intro-brand = { -brand-short-name } только что обновился в фоновом режиме. Щёлкните «Перезапустить { -brand-short-name }», чтобы завершить обновление.
restart-required-description = Мы сразу же восстановим все ваши страницы, окна и вкладки, так что вы сможете быстро продолжить свою работу.
restart-required-heading = Перезапустите для продолжения использования { -brand-short-name }
restart-required-intro = Обновление { -brand-short-name } запущено в фоновом режиме. Для завершения обновления вам потребуется перезапустить браузер.
window-restoration-info = Ваши окна и вкладки будут быстро восстановлены, кроме открытых в приватном режиме.
restart-button-label = Перезапустить { -brand-short-name }
