# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings for the upgrade dialog that can be displayed on major version change.


## New changes screen

upgrade-dialog-new-item-menu-description = Cuir béim ar nithe tábhachtacha chun gur féidir leat na nithe atá de dhíth ort a aimsiú.

## Pin Firefox screen
##
## These title, subtitle and button strings differ between platforms as they
## match the OS' application context menu item action where Windows uses "pin"
## and "taskbar" while macOS "keep" and "Dock" (proper noun).


## Default browser screen


## Theme selection screen


## Start screen


## Colorway screen


## Thank you screen

