# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, you can obtain one at http://mozilla.org/MPL/2.0/.

import-page-title = Uvozi

## Header

import-from-app = Uvozi iz programa
import-from-app-desc = Izberite uvoz računov, imenikov, koledarjev in drugih podatkov iz:
import-address-book = Uvozi datoteko z imenikom
import-calendar = Uvozi datoteko s koledarjem

## Buttons

button-cancel = Prekliči
button-back = Nazaj
button-continue = Nadaljuj

## Import from app steps

# Variables:
#   $app (String) - The name of the app to import from
profiles-pane-title = Uvozi iz programa { $app }
profiles-pane-desc = Izberite mesto, s katerega želite uvoziti podatke
profile-file-picker-dir = Izberite mapo s profilom
profile-file-picker-zip = Izberite datoteko .zip (manjšo od 2 GB)
items-pane-title = Izberite, kaj želite uvoziti
items-pane-desc = Uvozi iz
items-pane-checkbox-accounts = Račune in nastavitve
items-pane-checkbox-address-books = Imenike
items-pane-checkbox-calendars = Koledarje
items-pane-checkbox-mail-messages = Poštna sporočila

## Import dialog

progress-pane-title = Uvažanje
progress-pane-restart-desc = Znova zaženite za dokončanje uvoza.
error-pane-title = Napaka
error-message-zip-file-too-big = Izbrana datoteka .zip je večja od 2&nbsp;GB. Namesto uvoza jo ekstrahirajte, nato pa uvozite ekstrahirano mapo.
error-message-extract-zip-file-failed = Datoteke .zip ni bilo mogoče ekstrahirati. Ekstrahirajte jo ročno in nato uvozite iz ekstrahirane mape.
error-message-failed = Uvoz je nepričakovano spodletel. Več podatkov je morda na voljo v konzoli napak.
