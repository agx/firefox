# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = Zahtevan je ponoven zagon
restart-required-header = Oprostite. Za nadaljevanje moramo nekaj malega postoriti.
restart-required-intro-brand = { -brand-short-name } se je pravkar posodobil v ozadju. Kliknite Ponovno zaženi { -brand-short-name } za dokončanje posodobitve.
restart-required-description = Obnovili bomo vse vaše strani, okna in zavihke, tako da boste kmalu nazaj na želeni poti.
restart-required-heading = Ponovno zaženi pred nadaljnjo uporabo { -brand-short-name(sklon: "rodilnik") }
restart-required-intro = V ozadju se je začela posodobitev { -brand-short-name(sklon: "rodilnik") }. Za dokončanje posodobitve ga boste morali znova zagnati.
window-restoration-info = Vaša okna in zavihki se bodo hitro obnovili, zasebni pa se ne bodo.
restart-button-label = Ponovno zaženi { -brand-short-name }
