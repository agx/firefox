# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Window controls

about-rights-notification-text = { -brand-short-name } је слободан и отворен софтвер који је изградила заједница више хиљада особа из целог света.

## Content tabs


## Toolbar

quick-filter-toolbarbutton =
    .label = Брзо филтрирање
    .tooltiptext = Филтрира поруке
redirect-msg-button =
    .label = Преусмерење
    .tooltiptext = Преусмери изабрану поруку

## Folder Pane

folder-pane-header-label = Фасцикле

## Folder Toolbar Header Popup

folder-toolbar-hide-toolbar-toolbarbutton =
    .label = Сакриј алатницу
    .accesskey = и
show-all-folders-label =
    .label = Све фасцикле
    .accesskey = в
show-unread-folders-label =
    .label = Непрочитане фасцикле
    .accesskey = Н
show-favorite-folders-label =
    .label = Омиљене фасцикле
    .accesskey = ф
show-smart-folders-label =
    .label = Сједињене фасцикле
    .accesskey = ј
show-recent-folders-label =
    .label = Скорашње фасцикле
    .accesskey = к
folder-toolbar-toggle-folder-compact-view =
    .label = Сажети преглед
    .accesskey = г

## Menu

redirect-msg-menuitem =
    .label = Преусмери
    .accesskey = у

## AppMenu

# Since v89 we dropped the platforms distinction between Options or Preferences
# and consolidated everything with Preferences.
appmenu-preferences =
    .label = Поставке
appmenu-settings =
    .label = Подешавања
appmenu-addons-and-themes =
    .label = Додаци и теме
appmenu-redirect-msg =
    .label = Преусмери

## Context menu

context-menu-redirect-msg =
    .label = Преусмери

## Message header pane

other-action-redirect-msg =
    .label = Преусмери

## Action Button Context Menu


## Message headers


## Add-on removal warning


## no-reply handling


## error messages


## Spaces toolbar

spaces-toolbar =
    .toolbarname = Просторна трака
spaces-toolbar-button-mail =
    .title = Пребаци се на пошту
spaces-toolbar-button-address-book =
    .title = Пребаци се на именик
spaces-toolbar-button-calendar =
    .title = Пребаци се на календар
spaces-toolbar-button-tasks =
    .title = Пребаци се на задатке
spaces-toolbar-button-chat =
    .title = Пребаци се на ћаскање
spaces-toolbar-button-settings =
    .title = Пребаци се на подешавања
spaces-toolbar-button-collapse =
    .title = Скупи просторну траку
