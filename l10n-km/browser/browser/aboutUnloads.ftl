# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Strings used in about:unloads, allowing users to manage the "tab unloading"
### feature.

about-unloads-page-title = មិនកំពុងផ្ទុក​ផ្ទាំង
about-unloads-intro-1 =
    { -brand-short-name } មាន​មុខងារ​ដែល​មិន​ផ្ទុក​ផ្ទាំង​ដោយស្វ័យប្រវត្តិ
    ដើម្បី​ការពារ​កុំឱ្យ​កម្មវិធី​គាំង ដោយសារ​មាន​អង្គចងចាំ​មិន​គ្រប់គ្រាន់
    នៅពេល​អង្គចងចាំ​ដែល​មាន​របស់ប្រព័ន្ធ​ទាប។ ផ្ទាំង​បន្ទាប់​​ត្រូវបាន​ផ្ទុកឡើង
    ដោយផ្អែកតាម​លក្ខណៈច្រើន។ ទំព័រ​នេះ​បង្ហាញ​ពីរបៀប​ដែល
    { -brand-short-name } កំណត់អាទិភាព​ផ្ទាំង និង​ផ្ទាំងណា​នឹង​ត្រូវ​ឈប់ផ្ទុក
    នៅពេល​បើក​ការឈប់ផ្ទុក​ផ្ទាំង។
