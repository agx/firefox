# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

calendars-table-readonly = Sólo lectura
calendars-table-imip-identity = Identidad iMIP
calendars-table-imip-identity-disabled = iMIP desactivado
calendars-table-imip-identity-account = Cuenta iMIP
calendars-table-organizerid = Id del organizador
calendars-table-forceemailscheduling = Forzar programación de correos
