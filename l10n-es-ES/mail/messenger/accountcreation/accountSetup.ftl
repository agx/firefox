# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Header

account-setup-success-title = Cuenta creada correctamente

## Form fields

account-setup-password-toggle-hide =
    .title = Ocultar contraseña

## Action buttons


## Notifications


## Illustrations

account-setup-step1-image =
    .title = Configuración inicial
account-setup-step2-image =
    .title = Cargando…
account-setup-step3-image =
    .title = Configuración encontrada
account-setup-step4-image =
    .title = Error de conexión
account-setup-step5-image =
    .title = Cuenta creada
account-setup-privacy-footnote2 = Sus credenciales solo se almacenarán localmente en su ordenador.
account-setup-selection-error = ¿Necesita ayuda?

## Results area

# Note: IMAP is the name of a protocol.
account-setup-result-imap = IMAP
account-setup-result-imap-description = Mantener sus carpetas y correos electrónicos sincronizados en su servidor
# Note: POP3 is the name of a protocol.
account-setup-result-pop = POP3
account-setup-result-pop-description = Mantener sus carpetas y correos electrónicos en su equipo
account-setup-exchange-title = Servidor

## Error messages


## Manual configuration area

account-setup-hostname-label = Nombre del servidor:
account-setup-port-label = Puerto:
    .title = Establecer el puerto a 0 para la detección automática
account-setup-ssl-label = Seguridad de la conexión:

## Incoming/Outgoing SSL Authentication options

ssl-cleartext-password-option = Contraseña normal
ssl-encrypted-password-option = Contraseña cifrada

## Incoming/Outgoing SSL options


## Warning insecure server dialog


## Warning Exchange confirmation dialog


## Dismiss account creation dialog


## Alert dialogs


## Addon installation section


## Success view


## Calendar synchronization dialog

