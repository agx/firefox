# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

account-provisioner-tab-title = Obtener una nueva dirección de correo electrónico de un proveedor de servicios
provisioner-searching-icon =
    .alt = Buscando…
account-provisioner-title = Crear una nueva dirección de correo electrónico
account-provisioner-description = Utilice los servicios de nuestros socios de confianza para obtener una nueva dirección de correo electrónico privada y segura

## Forms

account-provisioner-search-button = Buscar
account-provisioner-button-cancel = Cancelar
account-provisioner-button-existing = Utilizar una cuenta de correo electrónico existente
account-provisioner-button-back = Retroceder

## Notifications

account-provisioner-fetching-provisioners = Obteniendo proveedores…

## Illustrations


## Search results

